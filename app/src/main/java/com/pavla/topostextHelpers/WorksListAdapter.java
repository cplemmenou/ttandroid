package com.pavla.topostextHelpers;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.pavla.topostext.R;

public class WorksListAdapter extends ArrayAdapter<String>{
	private Activity activity;
	private ArrayList<String> info;
	public ArrayList<String> works_filtered_array;
	private LayoutInflater inflater = null;  	
	public TextView placeNameTextView;
	//LinearLayout item_image;
	//TextView info_text;
	View view;
	
	public WorksListAdapter(Activity act, int resource, ArrayList<String> arrayList) {
		super(act, resource, arrayList);
		this.activity = act;
		this.info = arrayList;
		this.works_filtered_array = arrayList;
		 inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 
	}
	
	
	public ArrayList<String> getFilteredArray(){
		return works_filtered_array;
	}

	@Override
    public int getCount (){
        return works_filtered_array.size();
    }
	
	public String getItem(int position){
		return works_filtered_array.get(position);
	}


	@SuppressWarnings("deprecation")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
view = convertView;
		
		if (view == null) {
			
			view = inflater.inflate(R.layout.work_name_row, parent, false);
		} 

		
		if ((info == null) || ((position + 1) > works_filtered_array.size()))
			return view;
		
		final String placeName = works_filtered_array.get(position);
		
		//item_image=(LinearLayout)view.findViewById(R.id.side_image);
		placeNameTextView = (TextView) view.findViewById(R.id.workTitle);
		//Typeface mycustomFont = Typeface.createFromAsset(activity.getAssets(), "fonts/NotoSerif-Regular.ttf");
		//placeNameTextView.setTypeface(mycustomFont);
		placeNameTextView.setText(placeName);

		
		return view;
	}
	
	@Override
    public Filter getFilter() {

        Filter filter = new Filter() {


			@SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

				works_filtered_array = (ArrayList<String>) results.values;
                notifyDataSetChanged();
                //filterSelection = INITIAL;
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<String> FilteredArrayPlaces = new ArrayList<String>();
                
                // perform your search here using the searchConstraint String.
            	constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < info.size(); i++) {
                    String placeName = info.get(i);
                    String filter;
                    
                    filter = placeName;

                    if (filter.toLowerCase().contains(constraint.toString()))  {
                    	FilteredArrayPlaces.add(placeName);
                    }
                    
                }
                
                if	(constraint.toString().length() == 0)
                	FilteredArrayPlaces = info;
               
                
                results.count = FilteredArrayPlaces.size();
                results.values = FilteredArrayPlaces;

                return results;
            }
        };

        return filter;
    }
	
	
}


