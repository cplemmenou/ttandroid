package com.pavla.topostextHelpers;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pavla.topostext.IndexTableActivity;
import com.pavla.topostext.PlacesListFragment;
import com.pavla.topostext.R;

public class PlacesListAdapter extends ArrayAdapter<String> implements View.OnClickListener {
	private Activity activity;
	private ArrayList<String> info;
	public ArrayList<String> places_filtered;
	private LayoutInflater inflater = null;  	
	public TextView placeNameTextView;
	private final Context mContext;
	private MyListener ml;
	//LinearLayout item_image;
	//TextView info_text;
	View view;
	protected ListView mListview;
	
	public PlacesListAdapter(Activity act, int resource, ArrayList<String> arrayList,Context context,MyListener ml,ListView lv) {
		super(act, resource, arrayList);
		this.activity = act;
		this.info = arrayList;
		this.places_filtered = arrayList;
		 inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mContext = context;
		this.ml = ml;
		this.mListview = lv;
	}
	
	public ArrayList<String> getFilteredArray(){
		return places_filtered;
	}

	@Override
    public int getCount (){
        return places_filtered.size();
    }
	
	public String getItem(int position){
		return places_filtered.get(position);
	}
	


	@SuppressWarnings("deprecation")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
view = convertView;
		Log.i("LOG","BUILDING VIEW");

		
		if (view == null) {
			
			view = inflater.inflate(R.layout.place_name_row, parent, false);
		} 

		
		if ((places_filtered == null) || ((position + 1) > places_filtered.size()))
			return view;
		
		final String placeName = places_filtered.get(position);


		
		//item_image=(LinearLayout)view.findViewById(R.id.side_image);
		placeNameTextView = (TextView) view.findViewById(R.id.placeName);
		//Typeface mycustomFont = Typeface.createFromAsset(activity.getAssets(), "fonts/NotoSerif-Regular.ttf");
		//placeNameTextView.setTypeface(mycustomFont);
		placeNameTextView.setText(placeName);

		boolean tabletSize =  mContext.getResources().getBoolean(R.bool.isTablet);
		if (!tabletSize){
			ImageButton mapButton = (ImageButton) view.findViewById(R.id.mapButton);
			ImageButton listButton = (ImageButton) view.findViewById(R.id.listButton);
			mapButton.setTag(position);
			listButton.setTag(position);
			mapButton.setOnClickListener(this);
			listButton.setOnClickListener(this);
		}

		
		return view;
	}

	
	
	@Override
    public Filter getFilter() {

        Filter filter = new Filter() {


			@SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

				places_filtered = (ArrayList<String>) results.values;
                notifyDataSetChanged();
                //filterSelection = INITIAL;
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<String> FilteredArrayPlaces = new ArrayList<String>();
                
                // perform your search here using the searchConstraint String.
            	constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < info.size(); i++) {
                    String placeName = info.get(i);
                    String filter;
                    
                    filter = placeName;

                    if (filter.toLowerCase().contains(constraint.toString()))  {
                    	FilteredArrayPlaces.add(placeName);
                    }
                    
                }
                
                if	(constraint.toString().length() == 0)
                	FilteredArrayPlaces = info;
               
                
                results.count = FilteredArrayPlaces.size();
                results.values = FilteredArrayPlaces;

                return results;
            }
        };

        return filter;
    }


	@Override
	public void onClick(View v) {
		ImageButton myClickedButton = (ImageButton) v;
		switch (v.getId()){
			case R.id.listButton:
                final int position = mListview.getPositionForView((View) v.getParent());
                Intent intent = new Intent(getContext(),IndexTableActivity.class);
				String cleanTitle = places_filtered.get(position).replace("'","''");
				//Log.e("CLEAN TITLE","is : "+cleanTitle);
				IndexTableActivity.placeName = cleanTitle;
                //intent.putExtra("placename", "Abai");
                getContext().startActivity(intent);
                break;
			case R.id.mapButton:
				final int position2 = mListview.getPositionForView((View) v.getParent());
				ml.clickedPlace(places_filtered.get(position2));
				break;
		}
	}
}


