package com.pavla.topostextHelpers;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.pavla.ToposTextDatabase.IndexTable;
import com.pavla.ToposTextDatabase.Place;

public class IndexTableLoading extends AsyncTask<Place, Integer, ArrayList<IndexTable>> {
	String result = "";
	String status = "";
	Activity mContext;
	String message ="";
	boolean success = false;
	SQLiteDatabase database;
	ArrayList<IndexTable> myList;
	HashMap<String,ArrayList<IndexTable>> myHashmap;
	ArrayList<IndexTable> mySubList;
	IndexReadyListener myListener;
	ProgressBar progressbar;
	
	    
    //A good practice is to define field names as constants
	private static final String TABLE_NAME = "ZINDEXTABLE";
	private static final String TABLE_SECOND = "ZWORKS";
	private static final String TABLE_THIRD = "ZPARAGRAPH";
	private static final String EDATE = "ZEDATE";
	private static final String WDATE = "ZWDATE";
	private static final String NAME = "ZNAME";
    private static final String SCORE = "ZSCORE";
    private static final String PARAGRAPH = "ZPARAGRAPH";
    private static final String PARAGRAPH_LOCATION = "ZLOCATION";
    private static final String PLACE_ID = "ZPLACE";
    private static final String WORK_ID = "ZWORK";
    private static final String CONTEXT = "ZCONTEXT";
    private static final String TYPE = "ZTYPE";
    private static final String WORK_AUTHOR = "ZTITLE";
    private static final String WORK_WORK_ID = "_id";
    private static final String PARAGRAPH_ID = "_id";

	
	public  IndexTableLoading(Activity contex,SQLiteDatabase database,IndexReadyListener myListener,ProgressBar progressbar)
    {
     this.mContext=contex;
     this.database = database;
     this.myListener = myListener;
     this.progressbar = progressbar;
    }
	
	
	@Override
	protected ArrayList<IndexTable> doInBackground(Place... params) {
		Place currentplace = params[0];
		myList = new ArrayList<IndexTable>();
		
		myHashmap = new HashMap<String,ArrayList<IndexTable>>();
		//routes = new ArrayList<String>();
	    Cursor routesCursor = database.query(TABLE_NAME + " INNER JOIN "+TABLE_SECOND+" ON "+TABLE_NAME+"."+WORK_ID+" = "+TABLE_SECOND+"."+WORK_WORK_ID
	    		, new String[] {
	    		TABLE_NAME+"._id",TABLE_NAME+"."+EDATE,WORK_AUTHOR,CONTEXT,TYPE,NAME,TABLE_NAME+"."+WORK_ID,PARAGRAPH}, PLACE_ID +" = "+currentplace.getPk(), null, null, null, TABLE_NAME+"._id");
	        routesCursor.moveToFirst();
	        int ind = 1;
	        if(!routesCursor.isAfterLast()) {
	            do {
	            	int id = routesCursor.getInt(0);
	            	int edate = routesCursor.getInt(1);
	            	String workTitle = routesCursor.getString(2);
	            	String context = routesCursor.getString(3);
	                String type = routesCursor.getString(4);
	                String name = routesCursor.getString(5);
	                int workID = routesCursor.getInt(6);
					int paragraphID = routesCursor.getInt(7);
	                
	                this.progressbar.setProgress(100*ind/routesCursor.getCount());
	                
	                IndexTable currentIndex = new IndexTable(id,edate,"",workTitle,context,type,name,0,workID,currentplace.getPk());
					currentIndex.setParagraph(paragraphID);
	                myList.add(currentIndex);

	                ind++;
	            } while (routesCursor.moveToNext());
	        }
	        routesCursor.close();
	        return myList;
	}
	
	protected void onPreExecute(){
		super.onPreExecute();
	}
	
	protected void onProgressUpdate(Integer... progress){
		super.onProgressUpdate(progress);
	}
	
	protected void onPostExecute(ArrayList<IndexTable> message){
		super.onPostExecute(message);
		myListener.indexReady(message);
		//myListener.expandableIndexReady(message,myHashmap);
	}
}
