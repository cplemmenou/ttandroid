package com.pavla.topostextHelpers;

import java.util.ArrayList;
import java.util.HashMap;

import com.pavla.ToposTextDatabase.IndexTable;

public interface IndexReadyListener {
	void indexReady(ArrayList<IndexTable> theList);
	void expandableIndexReady(ArrayList<IndexTable> theList, HashMap<String, ArrayList<IndexTable>> theHashmap);
}
