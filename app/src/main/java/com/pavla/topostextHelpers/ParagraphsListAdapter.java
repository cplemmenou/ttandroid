package com.pavla.topostextHelpers;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pavla.topostext.R;

public class ParagraphsListAdapter extends ArrayAdapter<String>{
	private Activity activity;
	private ArrayList<String> info;
	private LayoutInflater inflater = null;  	
	public TextView paragraph;
	private String currentPlace,currentPlaceID;
	//LinearLayout item_image;
	//TextView info_text;
	View view;
	
	public ParagraphsListAdapter(Activity act, int resource, ArrayList<String> arrayList, String currentPlace, String currentPlaceID) {
		super(act, resource, arrayList);
		this.activity = act;
		this.info = arrayList;
		this.currentPlace = currentPlace;
		this.currentPlaceID = currentPlaceID;
		 inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 
	}
	


	@SuppressWarnings("deprecation")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
view = convertView;
		
		if (view == null) {
			
			view = inflater.inflate(R.layout.paragraph_row, parent, false);
		} 
		
		String paragraphString = info.get(position);
		
		//item_image=(LinearLayout)view.findViewById(R.id.side_image);
		paragraph = (TextView) view.findViewById(R.id.currentParagraph);
		Typeface mycustomFont = Typeface.createFromAsset(activity.getAssets(), "fonts/NotoSerif-Regular.ttf");
		paragraph.setTypeface(mycustomFont);
		paragraph.setLinkTextColor(Color.BLACK);
		paragraphString = paragraphString.replace("<a href=\"didtap://"+currentPlaceID+"\">"+currentPlace+"</a>", "<font color='blue'><a href=\"didtap://"+currentPlaceID+"\">"+currentPlace+"</a></font>");
		paragraph.setText(noTrailingwhiteLines(Html.fromHtml(paragraphString)));
		//setTextViewHTML(paragraph,paragraphString);
		paragraph.setMovementMethod(LinkMovementMethod.getInstance());
		paragraph.setLinksClickable(true);
		
		return view;
	}

	private CharSequence noTrailingwhiteLines(CharSequence text) {

	    while (text.charAt(text.length() - 1) == '\n') {
	        text = text.subSequence(0, text.length() - 1);
	    }
	    return text;
	}
	
}


