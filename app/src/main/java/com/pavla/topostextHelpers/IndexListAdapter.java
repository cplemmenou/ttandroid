package com.pavla.topostextHelpers;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.text.Html;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.pavla.ToposTextDatabase.ExternalDbOpenHelper;
import com.pavla.ToposTextDatabase.IndexTable;
import com.pavla.topostext.R;

public class IndexListAdapter extends ArrayAdapter<IndexTable>{
	private static final String DB_NAME = "ToposTextDatabaseAndroid.sqlite";
	private SQLiteDatabase database;
	private Activity activity;
	private ArrayList<IndexTable> info;
	private ArrayList<IndexTable> infoFilterable;
	private LayoutInflater inflater = null;
	public int totalHits;
	public TextView paragraphNumber,indexContent,author,moreLabel;
	//LinearLayout item_image;
	//TextView info_text;
	View view;
	
	private static final String TABLE_SECOND = "ZWORKS";
    private static final String WORK_WORK_ID = "_id";
    private static final String CATEGORY = "ZCATEGORY";
	private static final String LANGUAGE = "ZLANGUAGE";
	
	
	public IndexListAdapter(Activity act, int resource, ArrayList<IndexTable> arrayList) {
		super(act, resource, arrayList);
		this.activity = act;
		this.info = arrayList;
		this.totalHits = 0;
		this.infoFilterable = arrayList;
		 inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 
	}
	
	
	public void update(ArrayList<IndexTable> arrayList) {
        //this.firstlevellist = arrayList;
		
		this.infoFilterable = arrayList;
		
        //this.myHashmap = (ArrayList<ArrayList<String>>) _ChildStrings;
   }
	
	public int getCount (){
		totalHits = this.infoFilterable.size();
		return this.infoFilterable.size();
	}

	public ArrayList<IndexTable> getListItems(){
		return this.infoFilterable;
	}
	
	
    public IndexTable getItem(int position){
    	IndexTable cur = this.infoFilterable.get(position);	
    	totalHits += 1;
    	if (cur.getMore() > 0) totalHits += cur.getMore();
          return cur;
    }


	@SuppressWarnings("deprecation")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
view = convertView;
		
		if (view == null) {
			
			view = inflater.inflate(R.layout.index_row, parent, false);
		} 
		
		//final IndexTable myIndex = info.get(position);
		final IndexTable myIndex = infoFilterable.get(position);
		
		//item_image=(LinearLayout)view.findViewById(R.id.side_image);
		Typeface mycustomFont = Typeface.createFromAsset(activity.getAssets(), "fonts/NotoSerif-Regular.ttf");
		indexContent = (TextView) view.findViewById(R.id.currentIndexContent);
		indexContent.setTypeface(mycustomFont);
		author = (TextView) view.findViewById(R.id.author);
		

		author.setText(myIndex.getWorkTitle());
		int more = myIndex.getMore();

		author.setEllipsize(TruncateAt.END);
		String context = myIndex.getContext();
		context = context.replace("<b>", "").replace("</b>", "").replace(myIndex.getName(),"<font color='blue'>"+myIndex.getName()+"</font>");
		//indexContent.setText(myIndex.getContext());
		indexContent.setText(noTrailingwhiteLines(Html.fromHtml(context)));
		
		return view;
		
	}
	
	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void filterData (ArrayList<String> typeFilters, ArrayList<String> categoryFilters,ArrayList<String> languageFilters, String authorNameTitle){

        //this.infoFilterable = null;
		ArrayList<IndexTable> filteredArray = new ArrayList<IndexTable>();
		for (int j=0;j<typeFilters.size();j++){

			for (int i = 0; i<this.info.size();i++){
				IndexTable ind = this.info.get(i);
				if	(ind.getType().equalsIgnoreCase(typeFilters.get(j)) && (authorNameTitle == null || authorNameTitle == "" || ind.getWorkTitle().equals(authorNameTitle) || authorNameTitle.equals("ALL"))){
					filteredArray.add(ind);
				}
			}
		}

		for (int j=0;j<categoryFilters.size();j++){
			if	(typeFilters.size() > 0){
				ArrayList<IndexTable> shouldRemoveArray = new ArrayList<IndexTable>();
				for (int i = 0; i<filteredArray.size();i++){
					IndexTable ind = filteredArray.get(i);
					String workcategory = getWorkCategoryByWorkID(ind.getWork_id());
					if	(!workcategory.equalsIgnoreCase(categoryFilters.get(j)) || authorNameTitle == "" || authorNameTitle == null || (!ind.getWorkTitle().equals(authorNameTitle) && !authorNameTitle.equals("ALL"))){
						shouldRemoveArray.add(ind);
					}
				}
				filteredArray.removeAll(shouldRemoveArray);
			}else{
				for (int i = 0; i<this.info.size();i++){
					IndexTable ind = this.info.get(i);
					String workcategory = getWorkCategoryByWorkID(ind.getWork_id());
					if	(workcategory.equalsIgnoreCase(categoryFilters.get(j)) && (authorNameTitle == null || authorNameTitle == "" || authorNameTitle.equals("ALL") || ind.getWorkTitle().equals(authorNameTitle))){
						filteredArray.add(ind);
					}
				}
			}

		}

		for (int j=0;j<languageFilters.size();j++){
			if	(typeFilters.size() > 0 || categoryFilters.size() > 0){
				ArrayList<IndexTable> shouldRemoveArray = new ArrayList<IndexTable>();
				for (int i = 0; i<filteredArray.size();i++){
					IndexTable ind = filteredArray.get(i);
					String worklanguage = getWorkLanguageByWorkID(ind.getWork_id());
					if	(!worklanguage.equalsIgnoreCase(languageFilters.get(j)) || authorNameTitle == "" || authorNameTitle == null || (!ind.getWorkTitle().equals(authorNameTitle) && !authorNameTitle.equals("ALL"))){
						shouldRemoveArray.add(ind);
					}
				}
				filteredArray.removeAll(shouldRemoveArray);
			}else{
				for (int i = 0; i<this.info.size();i++){
					Log.i("LOG","AJKSDHF "+languageFilters.get(j));
					IndexTable ind = this.info.get(i);
					String worklanguage = getWorkLanguageByWorkID(ind.getWork_id());
					if	(worklanguage.equalsIgnoreCase(languageFilters.get(j)) && (authorNameTitle == null || authorNameTitle == "" || authorNameTitle.equals("ALL") || ind.getWorkTitle().equals(authorNameTitle))){
						Log.i("LOG","FILTERING....");
						filteredArray.add(ind);
					}
				}
			}
		}



		if	(typeFilters.size() == 0 && categoryFilters.size() == 0 && languageFilters.size() == 0){
			filteredArray = new ArrayList<>(this.info);

            //check authors filter if no others filters are checked
            ArrayList<IndexTable> temp = new ArrayList<IndexTable>();
            for (IndexTable obj : this.info){
                if (!obj.getWorkTitle().equals(authorNameTitle) && (authorNameTitle != null && authorNameTitle != "" && !authorNameTitle.equals("ALL"))) {
                    temp.add(obj);
                }
            }
            filteredArray.removeAll(temp);
		}


		Log.i("My logged array","array: "+filteredArray);

		this.infoFilterable = filteredArray;
		/*this.activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				notifyDataSetChanged();
			}
		});*/
	}

	
	private String getWorkCategoryByWorkID(int workID){
		//routes = new ArrayList<String>();
		ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(this.activity, DB_NAME);
		database = dbOpenHelper.openDataBase();
	    Cursor routesCursor = database.query(TABLE_SECOND, new String[] {
	    		CATEGORY}, WORK_WORK_ID +" = "+workID, null, null, null, null);
	        routesCursor.moveToFirst();
	        //if(!routesCursor.isAfterLast()) {
	        	String result = routesCursor.getString(0);
		        routesCursor.close();
		        database.close();
	        	return result;
	        //}else{
	        	//routesCursor.close();
	        	//database.close();
	        	//return null;
	        //}
	}

	private String getWorkLanguageByWorkID(int workID){
		//routes = new ArrayList<String>();
		ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(this.activity, DB_NAME);
		database = dbOpenHelper.openDataBase();
		Cursor routesCursor = database.query(TABLE_SECOND, new String[] {
				LANGUAGE}, WORK_WORK_ID +" = "+workID, null, null, null, null);
		routesCursor.moveToFirst();
		//if(!routesCursor.isAfterLast()) {
		String result = routesCursor.getString(0);
		routesCursor.close();
		database.close();
		return result;
		//}else{
		//routesCursor.close();
		//database.close();
		//return null;
		//}
	}
	
	private CharSequence noTrailingwhiteLines(CharSequence text) {

	    while (text.charAt(text.length() - 1) == '\n') {
	        text = text.subSequence(0, text.length() - 1);
	    }
	    return text;
	}
	
	
}


