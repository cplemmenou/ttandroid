package com.pavla.topostextHelpers;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.pavla.ToposTextDatabase.IndexTable;

public class MoreIndexTableLoading extends AsyncTask<IndexTable, Integer, ArrayList<IndexTable>> {
	String result = "";
	String status = "";
	Activity mContext;
	String message ="";
	boolean success = false;
	SQLiteDatabase database;
	ArrayList<IndexTable> myList;
	HashMap<String,ArrayList<IndexTable>> myHashmap;
	ArrayList<IndexTable> mySubList;
	IndexReadyListener myListener;
	ProgressBar progressbar;
	
	    
    //A good practice is to define field names as constants
	private static final String TABLE_NAME = "ZINDEXTABLE";
	private static final String TABLE_SECOND = "ZWORKS";
	private static final String TABLE_THIRD = "ZPARAGRAPH";
	private static final String EDATE = "ZEDATE";
	private static final String WDATE = "ZWDATE";
	private static final String NAME = "ZNAME";
    private static final String SCORE = "ZSCORE";
    private static final String PARAGRAPH = "ZPARAGRAPH";
    private static final String PARAGRAPH_LOCATION = "ZLOCATION";
    private static final String PLACE_ID = "ZPLACE";
    private static final String WORK_ID = "ZWORK";
    private static final String CONTEXT = "ZCONTEXT";
    private static final String TYPE = "ZTYPE";
    private static final String WORK_AUTHOR = "ZTITLE";
    private static final String WORK_WORK_ID = "_id";
    private static final String PARAGRAPH_ID = "_id";

	
	public  MoreIndexTableLoading(Activity contex,SQLiteDatabase database,IndexReadyListener myListener,ProgressBar progressbar)
    {
     this.mContext=contex;
     this.database = database;
     this.myListener = myListener;
     this.progressbar = progressbar;
    }
	
	
	@Override
	protected ArrayList<IndexTable> doInBackground(IndexTable... params) {
		IndexTable currentindex = params[0];
		myList = new ArrayList<IndexTable>();
		
		myHashmap = new HashMap<String,ArrayList<IndexTable>>();
		//routes = new ArrayList<String>();
		Cursor routesCursor = database.query(TABLE_NAME + " INNER JOIN "+TABLE_SECOND+" ON "+TABLE_NAME+"."+WORK_ID+" = "+TABLE_SECOND+"."+WORK_WORK_ID
	    		+" INNER JOIN "+TABLE_THIRD+" ON "+TABLE_NAME+"."+PARAGRAPH+" = "+TABLE_THIRD+"."+PARAGRAPH_ID
	    		, new String[] {
	    		TABLE_NAME+"._id",TABLE_NAME+"."+EDATE,PARAGRAPH_LOCATION,WORK_AUTHOR,CONTEXT,TYPE,NAME,TABLE_NAME+"."+WORK_ID}, 
	    		PLACE_ID +" = "+currentindex.getPlace_id()+" AND "+TABLE_NAME+"."+WORK_ID+" = "+currentindex.getWork_id(), 
	    		null, null, null, TABLE_NAME+"."+WDATE);
	        routesCursor.moveToFirst();
	        int ind = 1;
	        if(!routesCursor.isAfterLast()) {
	            do {
	            	int id = routesCursor.getInt(0);
	            	int edate = routesCursor.getInt(1);
	            	String plocation = routesCursor.getString(2);
	            	String workTitle = routesCursor.getString(3);
	            	String context = routesCursor.getString(4);
	                String type = routesCursor.getString(5);
	                String name = routesCursor.getString(6);
	                int workID = routesCursor.getInt(7);
	                
	                //this.progressbar.setProgress(100*ind/routesCursor.getCount());
	                
	                IndexTable currentIndex = new IndexTable(id,edate,plocation,workTitle,context,type,name,0,workID,currentindex.getPlace_id());
	                myList.add(currentIndex);
	                
	               /* Cursor subcursor = database.query(TABLE_NAME + " INNER JOIN "+TABLE_SECOND+" ON "+TABLE_NAME+"."+WORK_ID+" = "+TABLE_SECOND+"."+WORK_WORK_ID
	        	    		+" INNER JOIN "+TABLE_THIRD+" ON "+TABLE_NAME+"."+PARAGRAPH+" = "+TABLE_THIRD+"."+PARAGRAPH_ID
	        	    		, new String[] {
	        	    		TABLE_NAME+"._id",TABLE_NAME+"."+EDATE,PARAGRAPH_LOCATION,WORK_AUTHOR,CONTEXT,TYPE,NAME,"COUNT("+WORK_AUTHOR+")"}, PLACE_ID +" = "+currentplace.getPk()
	        	    		+" AND "+TABLE_NAME+"."+WORK_ID+" = '"+routesCursor.getInt(8)+"'"
	        	    		, null, null, null, SCORE);*/
	                /*Cursor subcursor = database.query(TABLE_NAME + " INNER JOIN "+TABLE_SECOND+" ON "+TABLE_NAME+"."+WORK_ID+" = "+TABLE_SECOND+"."+WORK_WORK_ID
	        	    		+" INNER JOIN "+TABLE_THIRD+" ON "+TABLE_NAME+"."+PARAGRAPH+" = "+TABLE_THIRD+"."+PARAGRAPH_ID
	        	    		, new String[] {
	        	    		TABLE_NAME+"._id",TABLE_NAME+"."+EDATE,PARAGRAPH_LOCATION,WORK_AUTHOR,CONTEXT,TYPE,NAME,TABLE_NAME+"."+WORK_ID}, PLACE_ID +" = "+currentplace.getPk()+" AND "+TABLE_NAME+"."+WORK_ID+" = "+workID , null, null, null, null);
	                Log.d("TAG","SBCURSOR COUNT"+subcursor.getCount());
	                ArrayList<IndexTable> subl = new ArrayList<IndexTable>();
	                subcursor.moveToFirst();
	                if(!subcursor.isAfterLast()) {
	    	            do {
	    	            	int sub_id = subcursor.getInt(0);
	    	            	int sub_edate = subcursor.getInt(1);
	    	            	String sub_plocation = subcursor.getString(2);
	    	            	String sub_workTitle = subcursor.getString(3);
	    	            	String sub_context = subcursor.getString(4);
	    	                String sub_type = subcursor.getString(5);
	    	                String sub_name = subcursor.getString(6);
	    	                int work_ID = subcursor.getInt(7);
	    	                Log.d("TAG","SUBLIST SHOULD CONTAIN"+sub_workTitle + "context"+sub_context);
	    	                IndexTable subCurrentIndex = new IndexTable(sub_id,sub_edate,sub_plocation,sub_workTitle,sub_context,sub_type,sub_name,0,work_ID);
	    	                subl.add(subCurrentIndex);
	    	            } while (subcursor.moveToNext());
	    	         }
	                Log.d("TAG","sublist full is "+subl);
	                myHashmap.put(workTitle, subl);
	                Log.d("TAG","MY HASHMAP NOW"+myHashmap);
	                */
	                ind++;
	            } while (routesCursor.moveToNext());
	        }
	        routesCursor.close();
	        return myList;
	}
	
	protected void onPreExecute(){
		super.onPreExecute();
	}
	
	protected void onProgressUpdate(Integer... progress){
		super.onProgressUpdate(progress);
		//Log.i("i am progressing", "onProgressUpdate(): " + String.valueOf(progress[0]));
	}
	
	protected void onPostExecute(ArrayList<IndexTable> message){
		super.onPostExecute(message);
		myListener.indexReady(message);
		//myListener.expandableIndexReady(message,myHashmap);
	}
}
