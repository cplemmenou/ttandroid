package com.pavla.topostextHelpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.pavla.topostext.IndexTableActivity;
import com.pavla.topostext.R;

import java.util.ArrayList;

import static com.pavla.topostext.R.id.placeName;

public class PeopleListAdapter extends ArrayAdapter<String> {
	private Activity activity;
	private ArrayList<String> info;
	public ArrayList<String> people_filtered_array;
	private LayoutInflater inflater = null;
	public TextView personTextView;
	//LinearLayout item_image;
	//TextView info_text;
	View view;

	public PeopleListAdapter(Activity act, int resource, ArrayList<String> arrayList) {
		super(act, resource, arrayList);
		this.activity = act;
		this.info = arrayList;
		this.people_filtered_array = arrayList;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}


	public ArrayList<String> getFilteredArray(){
		return people_filtered_array;
	}

	@Override
	public int getCount (){
		return people_filtered_array.size();
	}

	public String getItem(int position){
		return people_filtered_array.get(position);
	}


	@SuppressWarnings("deprecation")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		view = convertView;

		if (view == null) {

			view = inflater.inflate(R.layout.work_name_row, parent, false);
		}


		if ((info == null) || ((position + 1) > people_filtered_array.size()))
			return view;

		final String placeName = people_filtered_array.get(position);

		//item_image=(LinearLayout)view.findViewById(R.id.side_image);
		personTextView = (TextView) view.findViewById(R.id.workTitle);
		//Typeface mycustomFont = Typeface.createFromAsset(activity.getAssets(), "fonts/NotoSerif-Regular.ttf");
		//placeNameTextView.setTypeface(mycustomFont);
		personTextView.setText(placeName);


		return view;
	}

	@Override
	public Filter getFilter() {

		Filter filter = new Filter() {


			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {

				people_filtered_array = (ArrayList<String>) results.values;
				notifyDataSetChanged();
				//filterSelection = INITIAL;
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {

				FilterResults results = new FilterResults();
				ArrayList<String> FilteredArrayPlaces = new ArrayList<String>();

				// perform your search here using the searchConstraint String.
				constraint = constraint.toString().toLowerCase();
				for (int i = 0; i < info.size(); i++) {
					String placeName = info.get(i);
					String filter;

					filter = placeName;

					if (filter.toLowerCase().contains(constraint.toString()))  {
						FilteredArrayPlaces.add(placeName);
					}

				}

				if	(constraint.toString().length() == 0)
					FilteredArrayPlaces = info;


				results.count = FilteredArrayPlaces.size();
				results.values = FilteredArrayPlaces;

				return results;
			}
		};

		return filter;
	}

}


