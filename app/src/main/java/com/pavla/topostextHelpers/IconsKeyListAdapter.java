package com.pavla.topostextHelpers;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pavla.topostext.R;

public class IconsKeyListAdapter extends ArrayAdapter<String>{
	private Activity activity;
	private ArrayList<String> info;
	private ArrayList<String> icons;
	private LayoutInflater inflater = null;  	
	public TextView placeNameTextView;
	//LinearLayout item_image;
	//TextView info_text;
	View view;
	
	public IconsKeyListAdapter(Activity act, int resource, ArrayList<String> arrayList,ArrayList<String>arrayList2) {
		super(act, resource, arrayList);
		this.activity = act;
		this.info = arrayList;
		this.icons = arrayList2;
		 inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 
	}

	public ArrayList<String> getIcons(){
        return this.icons;
    }

	
	@Override
    public int getCount (){
        return info.size();
    }

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
view = convertView;
		
		if (view == null) {
			
			view = inflater.inflate(R.layout.key_row, parent, false);
		} 

		
		if ((info == null) || ((position + 1) > info.size()))
			return view;
		

			final String firstName = info.get(position);
		if(!firstName.equals("ALL")){
			final String firstDrawableName = icons.get(position);

			
			//item_image=(LinearLayout)view.findViewById(R.id.side_image);
			TextView firstInfo = (TextView) view.findViewById(R.id.firsttitle);

			firstInfo.setText(firstName);

			
			ImageView firstImage = (ImageView) view.findViewById(R.id.fistImage);
			firstImage.setVisibility(View.VISIBLE);
			
			Resources res = activity.getResources();
            int resIDf = res.getIdentifier(firstDrawableName, "drawable", activity.getPackageName());
            Drawable drawablefirst = res.getDrawable(resIDf);
            firstImage.setImageDrawable(drawablefirst);
		}else{

            TextView firstInfo = (TextView) view.findViewById(R.id.firsttitle);
			ImageView firstImage = (ImageView) view.findViewById(R.id.fistImage);
			firstImage.setVisibility(View.GONE);

            firstInfo.setText("ALL");

        }

		
		return view;
	}
	
	private int getDrawableResourceIdByName (String icon_name){
		Resources resources = activity.getResources();
		final int resourceId = resources.getIdentifier(icon_name, "drawable", 
		   activity.getPackageName());
		return resourceId;

	}
	
	
}


