package com.pavla.topostextHelpers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.pavla.topostext.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by plemmenou on 19/04/2017.
 */

public class GetRequest extends AsyncTask<String, Integer, Integer> {
    private Context context;

    public GetRequest(Context context) { this.context = context; }

    protected Integer doInBackground(String... urls) {
        URL url = null;
        StringBuffer response = new StringBuffer();
        Integer status_code = 1;
        try {
            url = new URL(urls[0]);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.connect();
            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;

            while ((inputLine = rd.readLine()) != null) {
               response.append(inputLine);
            }
            Log.i("LOG","response message"+response);
            try {

                JSONObject obj = new JSONObject(response.toString());
                JSONObject statusObj = obj.getJSONObject("status");

                Log.d("My App", ""+statusObj.getInt("status_code"));
                status_code = statusObj.getInt("status_code");

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + response.toString() + "\"");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i("LOG","RESPONSE "+response.toString());
        return status_code;
    }

    protected void onProgressUpdate(Integer... progress) {
    }

    protected void onPostExecute(Integer status_code) {
        // this is executed on the main thread after the process is over
        // update your UI here
        //displayMessage(result);
        switch (status_code){
            case 0:
                Toast.makeText(this.context, this.context.getResources().getString(R.string.sucessfull_feedback),Toast.LENGTH_LONG).show();
                break;
            case 1:
                Toast.makeText(this.context, this.context.getResources().getString(R.string.fail_feedback),Toast.LENGTH_LONG).show();
                break;
        }

    }
}
