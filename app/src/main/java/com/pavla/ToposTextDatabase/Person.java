package com.pavla.ToposTextDatabase;

import static android.R.attr.description;

public class Person {
	private int pk;
	private String century;
	private String gender;
	private int hits;
	private String name;
	private String period;
	private String personID;
	private String wikiLink;
	
	public int getPk() {
		return pk;
	}
	public void setPk(int pk) {
		this.pk = pk;
	}

	public Person(int myid, String century,int hits,String name,String period,String personID,String wikiLink){
		this.pk = myid;
		this.name = name;
		this.century = century;
		this.hits = hits;
		this.period = period;
		this.personID = personID;
		this.wikiLink = wikiLink;
	}

	public String getCentury() {
		return century;
	}

	public void setCentury(String century) {
		this.century = century;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getPersonID() {
		return personID;
	}

	public void setPersonID(String personID) {
		this.personID = personID;
	}

	public String getWikiLink() {
		return wikiLink;
	}

	public void setWikiLink(String wikiLink) {
		this.wikiLink = wikiLink;
	}
}
