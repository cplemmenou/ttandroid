package com.pavla.ToposTextDatabase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;
import static android.webkit.ConsoleMessage.MessageLevel.LOG;
import static com.google.android.gms.wearable.DataMap.TAG;
import static com.pavla.topostext.MapActivity.MY_PREFS_NAME;

public class ExternalDbOpenHelper extends SQLiteOpenHelper {

     //Path to the device folder with databases
    public static String DB_PATH;

     //Database file name
    public static String DB_NAME;
    public SQLiteDatabase database;
    public final Context context;
    private static final int DATABASE_VERSION = 13;//11;
    private String SP_KEY_DB_VER = "db_version";

     public SQLiteDatabase getDb() {
        return database;
    }

     public ExternalDbOpenHelper(Context context, String databaseName) {
        super(context, databaseName, null, DATABASE_VERSION);
        this.context = context;
     //Write a full path to the databases of your application
     String packageName = context.getPackageName();
         //File dbFile = this.context.getDatabasePath(DB_NAME);
     /*DB_PATH = String.format("//data//data//%s//databases//", packageName); //this works on one tablet
         File dir = new File(DB_PATH);
         if(dir.exists() && dir.isDirectory()) {
             // do something here
             Log.i("LOG","fiLE EXIST");
         }else{
             Log.i("LOG","fiLE does not EXIST");
             DB_PATH = String.format("//data//data//%s//",packageName); //this works on the other
         }*/
         //File sd = Environment.getExternalStorageDirectory();
         //DB_PATH = sd.getPath();
         //Log.e("LOG","THE PATH IS: "+DB_PATH);
         //if (!sd.canWrite()) {
             if (android.os.Build.VERSION.SDK_INT >= 4.2) {
                 if (Build.VERSION.SDK_INT == 22 || Build.VERSION.SDK_INT == 21 || Build.VERSION.SDK_INT == 19) {
                     Log.i("LOG", "in here");
                     DB_PATH = String.format("//data//data//%s//", packageName); //this works on the other
                 } else {
                     DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
                 }
             } else {
                 DB_PATH = String.format("//data//data//%s//databases//", packageName); //this works on the other
             }
         /*}else{
             //USE EXTERNAL STORAGE DIRECTORY
             DB_PATH = "//data//"+packageName+"//databases//";
         }*/

         //DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
         DB_NAME = databaseName;
         initialize();
        openDataBase();
    }


    private void initialize() {
        boolean dbExist = checkDataBase();
        if (dbExist) {
            Log.i("LOG","DB EXIST");
            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(this.context);
            int dbVersion = prefs.getInt(SP_KEY_DB_VER, 1);
            if (DATABASE_VERSION != dbVersion) {
                Log.i("LOG","should update database");
                File dbFile = this.context.getDatabasePath(DB_NAME);
                Log.i("LOG","DB FILE PATH: "+dbFile.getPath());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(SP_KEY_DB_VER, DATABASE_VERSION);
                editor.commit();
                if (!dbFile.delete()) {
                    Log.i("LOG", "Unable to update database");
                }
            }
        }
        if (!dbExist) {
            Log.i("LOG","DB DOES NOT EXIST CREATE IT");
                createDataBase();
        }
    }

     //This piece of code will create a database if it���s not yet created
    public void createDataBase() {
        boolean dbExist = checkDataBase();
        
        if(dbExist){
            // By calling this method here onUpgrade will be called on a
            // writable database, but only if the version number has been increased
            Log.i("LOG","DB EXIST GET WRITABLE");
            this.getWritableDatabase();
        }

        dbExist = checkDataBase();
        
        if (!dbExist) {
            this.getReadableDatabase();
            Log.i("LOG","DB DOES NOT EXIST");
            try {
                copyDataBase();
                Log.i("LOG","COPIED DATABASE");
            } catch (IOException e) {
                Log.i("LOG","logging catch of an exception");
                //throw new Error("Error copying database! "+e.getMessage());
                ((Activity)context).runOnUiThread(new Runnable() {
                    public void run() {
                        Log.i("LOG","this should run on main thread!");
                        Toast.makeText(context,"Luck of space.Sorry application will kill itself!",Toast.LENGTH_LONG).show();
                        ((Activity)context).finish();
                    }
                });

            }
        } else {
        }
    }

    //Performing a database existence check
    private boolean checkDataBase() {
        /*SQLiteDatabase checkDb = null;
        try {
            String path = DB_PATH + DB_NAME;
            checkDb = SQLiteDatabase.openDatabase(path, null,
                          SQLiteDatabase.OPEN_READONLY);
        } catch (SQLException e) {
            Log.e(this.getClass().toString(), "Error while checking db");
        }
        //Android doesn���t like resource leaks, everything should 
        // be closed
        if (checkDb != null) {
            checkDb.close();
        }
        return checkDb != null;*/
    	File dbFile = new File(DB_PATH + DB_NAME);
        Log.i("LOG","CHECK DATABASE RETURNS "+dbFile.exists());
        return dbFile.exists();
    }

    //Method for copying the database
    private void copyDataBase() throws IOException {
        //Open a stream for reading from our ready-made database
        //The stream source is located in the assets
        InputStream externalDbStream = context.getAssets().open(DB_NAME);

         //Path to the created empty database on your Android device
        String outFileName = DB_PATH + DB_NAME;

         //Now create a stream for writing the database byte by byte
        OutputStream localDbStream = new FileOutputStream(outFileName);

         //Copying the database
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = externalDbStream.read(buffer)) > 0) {
            localDbStream.write(buffer, 0, bytesRead);
        }
        //Don���t forget to close the streams
        localDbStream.close();
        externalDbStream.close();
        Log.i("LOG","END OF COPYDATABASE");
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        Log.i("LOG","START OF OPEN DATABASE");
        String path = DB_PATH + DB_NAME;
        if (database == null) {
            createDataBase();
            try {
                database = SQLiteDatabase.openDatabase(path, null,
                        SQLiteDatabase.OPEN_READWRITE);
                Log.i("LOG", "DB = " + database);
            }catch (Exception e) {
                Log.i("LOG","logging catch of an exception for ");
                //throw new Error("Error copying database! "+e.getMessage());
            }
        }
        Log.i("LOG","END OF OPEN DATABASE");
        return database;
    }

    @Override
    public synchronized void close() {
        if (database != null) {
            database.close();
        }
        super.close();
    }
    @Override
    public void onCreate(SQLiteDatabase db) {}
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    	
    	if(newVersion > oldVersion){                    
            context.deleteDatabase(DB_NAME);
    	}
    	
    }
}