package com.pavla.ToposTextDatabase;

import java.io.Serializable;
import java.util.Comparator;

import android.os.Parcel;

public class IndexTable implements Serializable{
	private int pk;
	private int edate;
	private int score;
	private int wdate;
	//private int nextparagraph;
	private int paragraph;
	private int place_id;
	//private int previousparagraph;
	private int work_id;
	private String context;
	private String name;
	private String type;
	private String workTitle;
	private String plocation;
	private int more;
	
	
	public IndexTable(int myid, int edate,String plocation,String workTitle, String context, String type,String name,int more,int work_id,int place_id){
		this.pk = myid;
		this.setEdate(edate);
		this.setContext(context);
		this.setType(type);
		this.setWorkTitle(workTitle);
		this.setPlocation(plocation);
		this.setName(name);
		this.setMore(more);
		this.work_id = work_id;
		this.place_id = place_id;
	}
	
	public int getPk() {
		return this.pk;
	}
	public void setPk(int pk) {
		this.pk = pk;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getEdate() {
		return edate;
	}

	public void setEdate(int edate) {
		this.edate = edate;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getWdate() {
		return wdate;
	}

	public void setWdate(int wdate) {
		this.wdate = wdate;
	}

	/*
	public int getNextparagraph() {
		return nextparagraph;
	}

	public void setNextparagraph(int nextparagraph) {
		this.nextparagraph = nextparagraph;
	}*/

	public int getParagraph() {
		return paragraph;
	}

	public void setParagraph(int paragraph) {
		this.paragraph = paragraph;
	}

	public int getPlace_id() {
		return place_id;
	}

	public void setPlace_id(int place_id) {
		this.place_id = place_id;
	}

	/*public int getPreviousparagraph() {
		return previousparagraph;
	}

	public void setPreviousparagraph(int previousparagraph) {
		this.previousparagraph = previousparagraph;
	}|*/

	public int getWork_id() {
		return work_id;
	}

	public void setWork_id(int work_id) {
		this.work_id = work_id;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getWorkTitle() {
		return workTitle;
	}

	public void setWorkTitle(String workTitle) {
		this.workTitle = workTitle;
	}

	public String getPlocation() {
		return plocation;
	}

	public void setPlocation(String plocation) {
		this.plocation = plocation;
	}

	public int getMore() {
		return more;
	}

	public void setMore(int more) {
		this.more = more;
	}
	
	
	/*Comparator for sorting the list by IndexTable*/
    public static Comparator<IndexTable> EventDateComparatorASC = new Comparator<IndexTable>() {

    	public int compare(IndexTable i1, IndexTable i2) {
    		int edate1 = i1.getEdate();
    		int edate2 = i2.getEdate();

    		//ascending order
    		return edate1-edate2;

    		//descending order
    		//return StudentName2.compareTo(StudentName1);
    	}
	};
	
	/*Comparator for sorting the list by IndexTable*/
    public static Comparator<IndexTable> EventDateComparatorDESC = new Comparator<IndexTable>() {

    	public int compare(IndexTable i1, IndexTable i2) {
    		int edate1 = i1.getEdate();
    		int edate2 = i2.getEdate();

    		//ascending order
    		return edate2-edate1;

    		//descending order
    		//return StudentName2.compareTo(StudentName1);
    	}
	};
	
	/*Comparator for sorting the list by */
    public static Comparator<IndexTable> AuthorComparatorASC = new Comparator<IndexTable>() {

    	public int compare(IndexTable i1, IndexTable i2) {
    		String authorName1 = i1.getWorkTitle().toUpperCase();
    		String authorName2 = i2.getWorkTitle().toUpperCase();
    		//ascending order
    		return authorName1.compareTo(authorName2);

    	}
	};
	
	/*Comparator for sorting the list by */
    public static Comparator<IndexTable> AuthorComparatorDESC = new Comparator<IndexTable>() {

    	public int compare(IndexTable i1, IndexTable i2) {
    		String authorName1 = i1.getWorkTitle().toUpperCase();
    		String authorName2 = i2.getWorkTitle().toUpperCase();
    		//ascending order
    		return authorName2.compareTo(authorName1);

    	}
	};
	
	/*Comparator for sorting the list by */
    public static Comparator<IndexTable> DefaultComparatorASC = new Comparator<IndexTable>() {

    	public int compare(IndexTable i1, IndexTable i2) {
    		int id1 = i1.getPk();
    		int id2 = i2.getPk();
    		//ascending order
    		return id1-id2;

    	}
	};
	
	/*Comparator for sorting the list by */
    public static Comparator<IndexTable> DefaultComparatorDESC = new Comparator<IndexTable>() {

    	public int compare(IndexTable i1, IndexTable i2) {
    		int id1 = i1.getPk();
    		int id2 = i2.getPk();
    		//ascending order
    		return id2-id1;

    	}
	};

}
