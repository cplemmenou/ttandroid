package com.pavla.ToposTextDatabase;

public class Place {
	private int pk;
	private int hits;
	private int priority;
	private int moderndescrcreditsid;
	private float latitude;
	private float longitude;
	private String confidence;
	private String description;
	private String name;
	private String featuretype;
	private String iconname;
	private String moderndescr;
	private String modernname;
	private String modernsource;
	private String placeid;
	private String timePeriod;
	private String traveloguesid;
	
	public Place(int myid, String name,String description,String confidence,int hits,float latitude,float longitude,String placeid){
		this.pk = myid;
		this.name = name;
		this.description = description;
		this.confidence = confidence;
		this.hits = hits;
		this.latitude = latitude;
		this.longitude = longitude;
		this.placeid = placeid;
	}
	
	public Place(int myid, String name,String description,String confidence,int hits,float latitude,float longitude,String placeid, String modernDescr,int moderndescrcreditsid,
			String traveloguesID){
		this.pk = myid;
		this.name = name;
		this.description = description;
		this.confidence = confidence;
		this.hits = hits;
		this.latitude = latitude;
		this.longitude = longitude;
		this.placeid = placeid;
		this.moderndescr = modernDescr;
		this.moderndescrcreditsid = moderndescrcreditsid;
		this.traveloguesid = traveloguesID;
	}
	
	public Place(String name,float latitude,float longitude,String icon_name){
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.iconname = icon_name;
	}
	
	public int getPk() {
		return pk;
	}
	public void setPk(int pk) {
		this.pk = pk;
	}
	public int getHits() {
		return hits;
	}
	public void setHits(int hits) {
		this.hits = hits;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public int getModerndescrcreditsid() {
		return moderndescrcreditsid;
	}
	public void setModerndescrcreditsid(int moderndescrcreditsid) {
		this.moderndescrcreditsid = moderndescrcreditsid;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public String getConfidence() {
		return confidence;
	}
	public void setConfidence(String confidence) {
		this.confidence = confidence;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFeaturetype() {
		return featuretype;
	}
	public void setFeaturetype(String featuretype) {
		this.featuretype = featuretype;
	}
	public String getIconname() {
		return iconname;
	}
	public void setIconname(String iconname) {
		this.iconname = iconname;
	}
	public String getModerndescr() {
		return moderndescr;
	}
	public void setModerndescr(String moderndescr) {
		this.moderndescr = moderndescr;
	}
	public String getModernname() {
		return modernname;
	}
	public void setModernname(String modernname) {
		this.modernname = modernname;
	}
	public String getModernsource() {
		return modernsource;
	}
	public void setModernsource(String modernsource) {
		this.modernsource = modernsource;
	}
	public String getPlaceid() {
		return placeid;
	}
	public void setPlaceid(String placeid) {
		this.placeid = placeid;
	}
	public String getTimePeriod() {
		return timePeriod;
	}
	public void setTimePeriod(String timePeriod) {
		this.timePeriod = timePeriod;
	}
	public String getTraveloguesid() {
		return traveloguesid;
	}
	public void setTraveloguesid(String traveloguesid) {
		this.traveloguesid = traveloguesid;
	}
	
}
