package com.pavla.topostext;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.pavla.topostextHelpers.MyListener;
import com.pavla.topostextHelpers.PeopleListAdapter;
import com.pavla.topostextHelpers.PlacesListAdapter;
import com.pavla.topostextHelpers.WorksListAdapter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.pavla.topostext.R.drawable.places;


public class PeopleListFragment extends Fragment implements View.OnClickListener,SearchView.OnQueryTextListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Map<String, Integer> mapIndex;
    View view;
    ListView peopleListview;
    SearchView mSearchView;
    ArrayList<String> peopleList;
    PeopleListAdapter listAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_people_list, container, false);



        //peopleList = (ArrayList<String>)getArguments().getSerializable("people");


        peopleListview = (ListView) view.findViewById(R.id.peopleListview);
        MapActivity myActivity = (MapActivity) getActivity();
        mSearchView = myActivity.getSearchView();
        mSearchView.setQuery("",false);
        peopleListview.setTextFilterEnabled(true);
        setupSearchView();

        //if	 (placesList.size() > 0){
        if	 (peopleList.size() > 0){
            listAdapter = new PeopleListAdapter(getActivity(), R.layout.work_name_row,peopleList);
            peopleListview.setAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();
        }

        peopleListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(getActivity(),PeopleIndexTableActivity.class);
                String cleanTitle = listAdapter.people_filtered_array.get(position).replace("'","''");
                PeopleIndexTableActivity.personName = cleanTitle;
                //intent.putExtra("personname",cleanTitle);
                startActivity(intent);
            }
        });
        //}

        /*if (tabletSize){
            peopleListview.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    //ml.clickedPlace(listAdapter.places_filtered.get(position));
                }
            });
        }*/


        getIndexList(peopleList);
        displayIndex();
        //mSearchView.clearFocus();

        return view;
    }

    public void updateListAdapter(ArrayList<String> updatedList){
        //Log.i("LOG","list updated with values : "+updatedList);
        //placesList.clear();
        //placesList = (ArrayList<String>) updatedList.clone();
        //	Log.i("LOG","list updated with values : "+placesList);
        listAdapter.notifyDataSetChanged();
    }


    private void getIndexList(ArrayList<String> people) {
        mapIndex = new LinkedHashMap<String, Integer>();
        for (int i = 0; i < people.size(); i++) {
            String fruit = people.get(i);
            String index = fruit.substring(0, 1);

            if (mapIndex.get(index) == null)
                mapIndex.put(index, i);
        }
    }
    private void setupSearchView() {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.clearFocus();
        //mSearchView.setFocusableInTouchMode(true);
        mSearchView.setQueryHint(getActivity().getResources().getString(R.string.search_people_hint));
    }

    private void displayIndex() {
        LinearLayout indexLayout = (LinearLayout) view.findViewById(R.id.indexView);
        TextView textView;
        List<String> indexList = new ArrayList<String>(mapIndex.keySet());
        for (String index : indexList) {
            textView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.side_index_item, null);
            //Typeface mycustomFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/NotoSerif-Regular.ttf");
            //textView.setTypeface(mycustomFont);
            textView.setText(index);
            textView.setOnClickListener(this);
            indexLayout.addView(textView);
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        TextView selectedIndex = (TextView) v;
        peopleListview.setSelection(mapIndex.get(selectedIndex.getText()));
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        PeopleListAdapter myAdapter = (PeopleListAdapter) peopleListview.getAdapter();
        myAdapter.getFilter().filter(newText);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String arg0) {
        // TODO Auto-generated method stub
        return false;
    }
}
