package com.pavla.topostext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.vision.text.Line;
import com.pavla.ToposTextDatabase.ExternalDbOpenHelper;
import com.pavla.ToposTextDatabase.IndexTable;
import com.pavla.ToposTextDatabase.Place;
import com.pavla.topostextHelpers.CustomMapTileProvider;
import com.pavla.topostextHelpers.GetRequest;
import com.pavla.topostextHelpers.IndexReadyListener;
import com.pavla.topostextHelpers.MoreIndexTableLoading;
import com.pavla.topostextHelpers.ParagraphsListAdapter;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;
import static android.os.Build.VERSION_CODES.M;
import static com.pavla.topostext.IndexTableActivity.isValidEmail;
import static com.pavla.topostext.R.id.currentParagraph;
import static com.pavla.topostext.R.id.indexList;
import static com.pavla.topostext.R.id.internalLayout;
//import static com.pavla.topostext.R.id.myLocationButton;
import static com.pavla.topostext.R.id.myLocationButton;
import static com.pavla.topostext.R.id.optionsButton;
import static java.lang.Boolean.FALSE;

public class FullWorkActivity extends AppCompatActivity implements OnMapReadyCallback, OnScrollListener, OnClickListener, IndexReadyListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
	private SQLiteDatabase database;
	private static final String DB_NAME = "ToposTextDatabaseAndroid.sqlite";
	private static final String TABLE_NAME = "ZPARAGRAPH";
	private static final String TEXTS_TABLE_NAME = "ZWORKS";
	private static final String PLACES_TABLE_NAME = "ZPLACES";
	private static final String PERSONS_TABLE_NAME = "ZPERSON";
	private static final String PERSON_NAME = "ZNAME";
	private static final String PERSON_ID = "ZPERSONID";
	private static final String WORK_ID_COLUMN = "ZWORK";
	private static final String PARAGRAPH_ID = "_id";
	private static final String CONTENT_COLUMN = "ZCONTENT";
	private static final String EDATE_COLUMN = "ZEDATE";
	private static final String GRCONTENT_COLUMN = "ZGRCONTENT";
	private static final String GREEKTITLE = "ZGREEKTITLE";
	private static final String LOCATION_COLUMN = "ZLOCATION";
	private static final String WDATE_COLUMN = "ZWDATE";
	private static final String CREDITS_COLUMN = "ZCREDITS";
	private static final String WORK_WORK_ID = "_id";
	private static final String WORK_TITLE_COLUMN = "ZTITLE";
	private static final String PLACE_ID_COLUMN = "ZPLACEID";
	private static final String LATITUDE_COLUMN = "ZLATITUDE";
	private static final String LONGITUDE_COLUMN = "ZLONGITUDE";
	private static final String PLACE_NAME_COLUMN = "ZDISPLAYNAME";
	private static final String ICON = "ZICON_NAME";

	private boolean isFromIndex;
	private String currentParagraphID;
	private int currentParagraphIndexNum = 0;
	private String currentPlaceID;
	private String currentPlaceName;

	private PopupWindow popupWindow;

	private LinkedHashMap<String, Integer> mapIndex;

	private ListView paragraphsListView;
	private CheckBox toggleLanguageButton;
	private TextView workTitle, hitsCounter, textsCounter;
	private Button driveMe, previousHit, nextHit;//,optionsButton;
	private LinearLayout optionsLayout, indexListLayout;
	private ProgressBar loadingView;
	private RelativeLayout mapLayout;
	private ScrollView indexScrollView;
	private ImageButton myLocButton;

	private TileOverlay offlineOverlay;

	ArrayList<String> theParagraphsList = new ArrayList<String>();
	ArrayList<String> theParagraphsNumbers = new ArrayList<String>();
	ArrayList<String> theGrParagraphsList = new ArrayList<String>();
	ArrayList<String> theFinalAdapterList = new ArrayList<String>();
	ArrayList<Integer> edateList = new ArrayList<Integer>();
	ArrayList<Integer> wdateList = new ArrayList<Integer>();
	//private ArrayList<IndexTable> textsList;
	private ArrayList<IndexTable> hitsList;
	private GoogleMap myMap;


	private static String workName;
	private static TextView datesTitle;
	private int firstItemVisiblePosition;

	//options buttons counters
	private int currentTextIndex;
	private int totalTexts;
	private int currentHitIndex;
	private int totalHits;
	private int childPosition;
	private int hitsnumber;


	private Boolean FIRST_MENU = true;
	private Boolean JUST_FULL_WORK_VIEW = true;

	private ParagraphsListAdapter listAdapter;

	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (Intent.ACTION_VIEW.equals(intent.getAction())) {
				Uri uri = intent.getData();

			}
		}
	};

	private IntentFilter filter;
	private GoogleApiClient mGoogleApiClient;

	@SuppressLint("WrongViewCast")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
		/*if (tabletSize) {
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			getSupportActionBar().hide();
		}else{*/
			/*getSupportActionBar().setIcon(R.drawable.logo_laskaridis_s);
			getSupportActionBar().setLogo(R.drawable.logo_laskaridis_s);*/
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().show();
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
		//}


		setContentView(R.layout.activity_full_work);

		Intent i = getIntent();
		currentHitIndex = i.getExtras().getInt("currentPosition");
		hitsnumber = currentHitIndex + 1;
		totalHits = i.getExtras().getInt("totalHits");
		if (i.hasExtra("placeName")) {
			currentPlaceName = i.getExtras().getString("placeName");
		}
		//int start = currentPlaceName.indexOf("(");
		//String suffix = currentPlaceName.substring(start + 1);
		getSupportActionBar().setTitle(currentPlaceName + "Hits " + hitsnumber + "/totalHits");

		if (!tabletSize) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			mapLayout = (RelativeLayout) findViewById(R.id.maplayout);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}


		MapFragment mapFragment = (MapFragment) getFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);

		ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(FullWorkActivity.this, DB_NAME);
		database = dbOpenHelper.openDataBase();

		isFromIndex = false;

		paragraphsListView = (ListView) findViewById(R.id.paragraphsList);
		workTitle = (TextView) findViewById(R.id.workTitle);
		datesTitle = (TextView) findViewById(R.id.dateRange);
		paragraphsListView.setOnScrollListener(this);
		toggleLanguageButton = (CheckBox) findViewById(R.id.toggleLanguageButton);
		toggleLanguageButton.setOnClickListener(this);
		ImageButton copyrightButton = (ImageButton) findViewById(R.id.copyrightButton);
		indexListLayout = (LinearLayout) findViewById(R.id.indexListLinear);
		copyrightButton.setOnClickListener(this);
		optionsLayout = (LinearLayout) findViewById(R.id.optionsLayout);
		myLocButton = (ImageButton) findViewById(R.id.myPositionButton);
		//optionsButton = (FloatingActionButton) findViewById(R.id.floatingOptionsButton);
		optionsLayout.setVisibility(View.GONE);

		indexListLayout.setVisibility(View.GONE);

		myLocButton.setOnClickListener(this);
		textsCounter = (TextView) findViewById(R.id.textCounter);
		hitsCounter = (TextView) findViewById(R.id.hitsCounter);
		driveMe = (Button) findViewById(R.id.driveMeButton);
		previousHit = (Button) findViewById(R.id.previousHitButton);
		nextHit = (Button) findViewById(R.id.nextHitButton);
		//previousText = (Button) findViewById(R.id.previousTextButton);
		//nextText = (Button) findViewById(R.id.nextTextButton);
		TextView hitsTitle = (TextView) findViewById(R.id.hitsTitle);
		//TextView textsTitle = (TextView) findViewById(R.id.textsTitle);
		loadingView = (ProgressBar) findViewById(R.id.loadingView);
		indexScrollView = (ScrollView) findViewById(R.id.indexScrollView);

		RadioButton onlineMap = (RadioButton) findViewById(R.id.online_map);
		RadioButton offlineMap = (RadioButton) findViewById(R.id.offline_map);
		RadioButton standardMap = (RadioButton) findViewById(R.id.standard_map);
		RadioButton satelliteMap = (RadioButton) findViewById(R.id.satellite_map);

		onlineMap.setOnClickListener(this);
		offlineMap.setOnClickListener(this);
		standardMap.setOnClickListener(this);
		satelliteMap.setOnClickListener(this);

		if (!tabletSize) {
			mapLayout = (RelativeLayout) findViewById(R.id.maplayout);
		}

		driveMe.setOnClickListener(this);
		//previousHit.setOnClickListener(this);
		//nextHit.setOnClickListener(this);


		if (i.hasExtra("myWorkName")) {
			workName = i.getExtras().getString("myWorkName");
			workTitle.setText(workName);
		}

		if (mGoogleApiClient == null) {
			mGoogleApiClient = new GoogleApiClient.Builder(this)
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this)
					.addApi(LocationServices.API)
					.build();
		}

		//Intent i = getIntent();

		if (i.hasExtra("paragraphID")) {
			currentParagraphID = i.getExtras().getString("paragraphID");
			isFromIndex = true;
			if (i.hasExtra("placeID")) {
				currentPlaceID = i.getExtras().getString("placeID");
			}
			JUST_FULL_WORK_VIEW = false;

			updateHitsCounter();

		} else {
			optionsLayout.setVisibility(View.GONE);
		}

		final boolean hasHitsList = i.hasExtra("hitsList");
		if (hasHitsList) {

			hitsList = (ArrayList<IndexTable>) i.getExtras().getSerializable("hitsList");
		}
		if (i.hasExtra("myWorkName")) {
			AsyncTask.execute(new Runnable() {
				@Override
				public void run() {

					if (theParagraphsList.size() <= 0) {
						getParagraphs(getWorkId());
					}


					if (theParagraphsList.size() > 0) {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (!hasGreekText()) {
									toggleLanguageButton.setVisibility(View.GONE);
								}

								listAdapter = new ParagraphsListAdapter(FullWorkActivity.this, R.layout.paragraph_row, theParagraphsList, currentPlaceName, currentPlaceID);
								paragraphsListView.setAdapter(listAdapter);
								listAdapter.notifyDataSetChanged();
							}
						});
					}

					if (mapIndex == null) {
						mapIndex = new LinkedHashMap<String, Integer>();
						getIndexList(theParagraphsNumbers);
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								displayIndex();
								if (isFromIndex) {
									paragraphsListView.setSelection(currentParagraphIndexNum);
								}

								loadingView.setVisibility(View.GONE);
							}
						});
					}


					if (wdateList.size() > 0 && edateList.size() > 0) {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								datesTitle.setText("Written ~" + wdateList.get(0) + ", events ~" + edateList.get(0));
							}
						});
					}

				}
			});

		}

	}

	protected void onStart() {
		super.onStart();


	}

	public void onStop() {
		super.onStop();
		if (getIntent().hasExtra("previousActivity")) {
			finish();
		}
	}
	
	/*private void calculateHits(){
		int currentHitsNum = 0;
		for (int j=0;j<textsList.size();j++){
			if	(j == currentTextIndex){
				currentHitsNum += currentHitIndex +1;
			}else if (j < currentTextIndex){
				IndexTable anIndexTable = textsList.get(j);
				currentHitsNum += anIndexTable.getMore() + 1;
			}else{
				break;
			}
		}
		hitsnumber = currentHitsNum;
	}*/
	
	/*private void calculateText(){
		int currentHitsNum = 0;
		for (int j=0;j<textsList.size();j++){
			IndexTable anIndexTable = (IndexTable)textsList.get(j);
			currentHitIndex = hitsnumber-currentHitsNum;
			currentHitsNum += 1 + anIndexTable.getMore();
			if	(hitsnumber < currentHitsNum){
				currentTextIndex = j;
				break;
			}
		}
		//currentHitIndex = currentHitsNum-1;
	}*/

	private void updateTextsCounter() {
		textsCounter.setText(currentTextIndex + 1 + "/" + totalTexts);
	}

	private void updateHitsCounter() {
		hitsCounter.setText(hitsnumber + "/" + totalHits);
	}

	private void getIndexList(ArrayList<String> paragraphs) {
		// mapIndex = new LinkedHashMap<String, Integer>();
		for (int i = 0; i < paragraphs.size(); i = i + 20) {
			String index = paragraphs.get(i);
			//String index = fruit.substring(0, 1);

			if (mapIndex.get(index) == null)
				mapIndex.put(index, i);
		}
	}


	private void displayIndex() {
		LinearLayout indexLayout = (LinearLayout) findViewById(indexList);
		indexLayout.removeAllViews();
		TextView textView;
		List<String> indexList = new ArrayList<String>(mapIndex.keySet());
		for (String index : indexList) {
			textView = (TextView) this.getLayoutInflater().inflate(R.layout.side_index_item, null);
			//Typeface mycustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/NotoSerif-Regular.ttf");
			//textView.setTypeface(mycustomFont);
			textView.setText(index);
			textView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TextView selectedIndex = (TextView) v;
					currentParagraphIndexNum = mapIndex.get(selectedIndex.getText());
					paragraphsListView.setSelection(mapIndex.get(selectedIndex.getText()));
				}
			});
			indexLayout.addView(textView);
		}
	}


	protected void onNewIntent(Intent intent) {
		if (Intent.ACTION_VIEW.equals(intent.getAction())) {
			Uri uri = intent.getData();
			String[] parts = uri.toString().split("//");
			if (!parts[1].contains("PRN=")) {
				//place clicked
				Place thePlace = getPlaceCoordinates(parts[1]);
				String iconname = thePlace.getIconname();
				iconname = iconname.replace(".png", "_ipad");
				BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(getDrawableResourceIdByName(iconname));

				LatLng latlng = new LatLng(thePlace.getLatitude(), thePlace.getLongitude());
				myMap.addMarker(new MarkerOptions()
						.position(latlng)
						.title(thePlace.getName())
						.icon(icon));

				myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 8));
			} else {
				String[] newparts = uri.toString().split("=");
				String personName = getPersonName(newparts[1]);
				Intent pIntent = new Intent(this, PeopleIndexTableActivity.class);
				PeopleIndexTableActivity.personName = personName;
				//pIntent.putExtra("personname", personName);
				pIntent.putExtra("previousActivity", "FullWork");
				startActivity(pIntent);

			}
		}
	}

	private int getDrawableResourceIdByName(String icon_name) {
		Resources resources = this.getResources();
		final int resourceId = resources.getIdentifier(icon_name, "drawable",
				this.getPackageName());
		return resourceId;

	}


	@Override
	protected void onResume() {
		// Set When broadcast event will fire.
		filter = new IntentFilter("android.intent.action.VIEW");
		// Register reciever if activity is in front
		this.registerReceiver(receiver, filter);
		super.onResume();
	}

	@Override
	protected void onPause() {

		// Unregister reciever if activity is not in front
		this.unregisterReceiver(receiver);
		super.onPause();
	}

	public void onDestroy() {
		super.onDestroy();

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		/*boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
		if (!tabletSize){
			if (FIRST_MENU)
				getMenuInflater().inflate(R.menu.map, menu);
			else
				getMenuInflater().inflate(R.menu.map_second, menu);
			return true;
		}else{
			getMenuInflater().inflate(R.menu.map, menu);
			return true;
		}*/
		return true;

	}


	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
		if (!tabletSize) {
			menu.clear();
			if (JUST_FULL_WORK_VIEW || currentPlaceID == null) {
				if (FIRST_MENU)
					getMenuInflater().inflate(R.menu.just_full_work, menu);
				else
					getMenuInflater().inflate(R.menu.just_full_work_second, menu);
			} else {
				if (FIRST_MENU)
					getMenuInflater().inflate(R.menu.full_work, menu);
				else
					getMenuInflater().inflate(R.menu.full_work_second, menu);
			}
			return super.onPrepareOptionsMenu(menu);
		} else {
			if (JUST_FULL_WORK_VIEW) {
				getMenuInflater().inflate(R.menu.just_full_work_tablet, menu);
			} else {
				getMenuInflater().inflate(R.menu.full_work_tablet, menu);
			}

			return super.onPrepareOptionsMenu(menu);
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.text) {
			FIRST_MENU = true;
			mapLayout.setVisibility(View.GONE);
			invalidateOptionsMenu();
			return true;
		} else if (id == R.id.map) {
			FIRST_MENU = false;
			mapLayout.setVisibility(View.VISIBLE);
			invalidateOptionsMenu();
		} else if (id == R.id.feedback) {
			showFeedbackpopup();
		} else if (id == R.id.paragraph) {
			if (indexListLayout.getVisibility() == View.VISIBLE)
				indexListLayout.setVisibility(View.GONE);
			else
				indexListLayout.setVisibility(View.VISIBLE);
		} else if (id == R.id.driveme) {
			new AlertDialog.Builder(this)
					.setMessage(getResources().getString(R.string.sure_drive_me))
					.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// continue with delete
							Place currentPlaceLocation = null;
							try {
								currentPlaceLocation = getPlaceCoordinates(currentPlaceID);
							} catch (Exception ex) {
								ex.printStackTrace();
							}
							Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + String.valueOf(currentPlaceLocation.getLatitude()) + "," + String.valueOf(currentPlaceLocation.getLongitude())));
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intent);
						}
					})
					.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// do nothing
						}
					})
					.setIcon(android.R.drawable.ic_dialog_alert)
					.show();
		} else if (id == R.id.nexthit) {
			if (hitsnumber < totalHits) {
				hitsnumber++;
				currentHitIndex++;
				//calculateText();
				updateHitsCounter();
				//updateTextsCounter();
				//IndexTable cIndex = hitsList.get(hitsnumber-1);
				//currentParagraphIndexNum = theParagraphsNumbers.indexOf("§"+cIndex.getPlocation());//mapIndex.get("§"+cIndex.getPlocation());
				//scrollToSelectedHit();
				updateView();
			}
		} else if (id == R.id.previoushit) {
			if (hitsnumber > 1) {
				hitsnumber--;
				currentHitIndex--;
				updateHitsCounter();
				//updateTextsCounter();
				//IndexTable cIndex = hitsList.get(hitsnumber-1);
				//currentParagraphIndexNum = theParagraphsNumbers.indexOf("§"+cIndex.getPlocation());//mapIndex.get("§"+cIndex.getPlocation());
				//scrollToSelectedHit();
				updateView();
			}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onMapReady(GoogleMap map) {
		// TODO Auto-generated method stub
		if (getIntent().hasExtra("placeID")) {
			currentPlaceID = getIntent().getExtras().getString("placeID");
		}
		map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		if (!isNetworkOnline()) {
			map.addTileOverlay(new TileOverlayOptions().tileProvider(new CustomMapTileProvider(getResources().getAssets())));
		}

		if (isFromIndex && currentPlaceID != null) {
			Place thePlace = getPlaceCoordinates(currentPlaceID);
			String iconname = thePlace.getIconname();
			iconname = iconname.replace(".png", "_ipad");
			iconname = iconname.replace("?", "q");
			BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(getDrawableResourceIdByName(iconname));

			if (thePlace != null) {
				LatLng latlng = new LatLng(thePlace.getLatitude(), thePlace.getLongitude());
				map.addMarker(new MarkerOptions()
						.position(latlng)
						.title(thePlace.getName())
						.icon(icon));

				map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 6));
			}
		} else {
			LatLng latlng = new LatLng(37.938424, 23.644326);
			/*map.addMarker(new MarkerOptions()
	        .position(latlng)
	        .title("Marker"));*/


			map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 6));
		}

		myMap = map;

	}

	public boolean isNetworkOnline() {
		boolean status = false;
		try {
			ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getNetworkInfo(0);
			if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
				status = true;
			} else {
				netInfo = cm.getNetworkInfo(1);
				if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED)
					status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return status;

	}

	private int getWorkId() {
		//routes = new ArrayList<String>();
		Cursor routesCursor = database.query(TEXTS_TABLE_NAME, new String[]{
				WORK_WORK_ID}, WORK_TITLE_COLUMN + " = '" + workName + "'", null, null, null, null);
		routesCursor.moveToFirst();
		int workID = routesCursor.getInt(0);
		return workID;
	}

	private String getWorkCredits() {
		Cursor routesCursor = database.query(TEXTS_TABLE_NAME, new String[]{
				CREDITS_COLUMN}, WORK_TITLE_COLUMN + " = '" + workName + "'", null, null, null, null);
		routesCursor.moveToFirst();
		String workCredits = routesCursor.getString(0);
		return workCredits;
	}

	private boolean hasGreekText() {
		Cursor routesCursor = database.query(TEXTS_TABLE_NAME, new String[]{
				GREEKTITLE}, WORK_TITLE_COLUMN + " = '" + workName + "'", null, null, null, null);
		routesCursor.moveToFirst();
		String greekTitle = routesCursor.getString(0);
		return greekTitle.length() > 0;
	}


	private void getParagraphs(int workID) {
		//routes = new ArrayList<String>();
		Cursor routesCursor = database.query(TABLE_NAME, new String[]{
				CONTENT_COLUMN, EDATE_COLUMN, GRCONTENT_COLUMN, LOCATION_COLUMN, WDATE_COLUMN, PARAGRAPH_ID}, WORK_ID_COLUMN + " = " + workID, null, null, null, PARAGRAPH_ID);
		routesCursor.moveToFirst();
		int i = 0;
		if (!routesCursor.isAfterLast()) {
			do {
				final String content = routesCursor.getString(0);
				final String contentgr = routesCursor.getString(2);
				final String location = routesCursor.getString(3);
				int wdate = routesCursor.getInt(4);
				int edate = routesCursor.getInt(1);
				int parag_id = routesCursor.getInt(5);

				//String image_name = routesCursor.getString(5);

				theGrParagraphsList.add("<p><b>§" + location + "</b> " + contentgr + "</p>");
				theParagraphsList.add("<p><b>§" + location + "</b> " + content + "</p>");
				theParagraphsNumbers.add("§" + location);


				//SOS
				if (currentParagraphID != null) {
					if (parag_id == Integer.parseInt(currentParagraphID)) {//if	((location).equalsIgnoreCase(currentParagraphID)) {
						currentParagraphIndexNum = i;
					}
				} else {
					currentParagraphIndexNum = 0;
				}

				wdateList.add(wdate);
				edateList.add(edate);
				i++;
			} while (routesCursor.moveToNext());
		}
		routesCursor.close();
	}

	private Place getPlaceCoordinates(String placeID) {
		Cursor routesCursor = database.query(PLACES_TABLE_NAME, new String[]{
				LATITUDE_COLUMN, LONGITUDE_COLUMN, PLACE_NAME_COLUMN, ICON}, PLACE_ID_COLUMN + " = '" + placeID + "'", null, null, null, null);
		routesCursor.moveToFirst();
		double lat = routesCursor.getDouble(0);
		double longit = routesCursor.getDouble(1);
		String placeName = routesCursor.getString(2);
		String iconName = routesCursor.getString(3);

		Place selectedPlace = new Place(placeName, (float) lat, (float) longit, iconName);
		LatLng latlng = new LatLng(lat, longit);
		return selectedPlace;
	}


	private String getPersonName(String personID) {
		Cursor routesCursor = database.query(PERSONS_TABLE_NAME, new String[]{
				PERSON_NAME}, PERSON_ID + " = '" + personID + "'", null, null, null, null);
		routesCursor.moveToFirst();

		String personName = routesCursor.getString(0);


		return personName;
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		if (scrollState == SCROLL_STATE_IDLE) {
			if (wdateList.size() > 0 && edateList.size() > 0) {
				String w, e;
				if (wdateList.get(firstItemVisiblePosition) > 0) {
					w = wdateList.get(firstItemVisiblePosition) + "AC";
				} else {
					w = -wdateList.get(firstItemVisiblePosition) + "BC";
				}

				if (edateList.get(firstItemVisiblePosition) > 0) {
					e = edateList.get(firstItemVisiblePosition) + "AC";
				} else {
					e = -edateList.get(firstItemVisiblePosition) + "BC";
				}

				datesTitle.setText("Written ~" + w + ", events ~" + e);
			}
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
						 int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		firstItemVisiblePosition = firstVisibleItem;


	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.toggleLanguageButton:
				if (toggleLanguageButton.isChecked()) {
					ParagraphsListAdapter listAdapter = new ParagraphsListAdapter(this, R.layout.paragraph_row, theGrParagraphsList, currentPlaceName, currentPlaceID);
					paragraphsListView.setAdapter(listAdapter);
					listAdapter.notifyDataSetChanged();
					paragraphsListView.setSelection(currentParagraphIndexNum);
				} else {
					if (theParagraphsList.size() > 0) {
						ParagraphsListAdapter listAdapter = new ParagraphsListAdapter(this, R.layout.paragraph_row, theParagraphsList, currentPlaceName, currentPlaceID);
						paragraphsListView.setAdapter(listAdapter);
						listAdapter.notifyDataSetChanged();
						paragraphsListView.setSelection(currentParagraphIndexNum);
					}
				}
				break;
			case R.id.copyrightButton:
			/*Intent i = new Intent(FullWorkActivity.this,CreditsPopupActivity.class);
			i.putExtra("creditsStatement", getWorkCredits());
			startActivity(i);*/
				showPopup();
				break;
			case R.id.clickableBackground:
				popupWindow.dismiss();
				break;
			case R.id.driveMeButton:
				new AlertDialog.Builder(this)
						.setMessage(getResources().getString(R.string.sure_drive_me))
						.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// continue with delete
								Place currentPlaceLocation = null;
								try {
									currentPlaceLocation = getPlaceCoordinates(currentPlaceID);
								} catch (Exception ex) {
									ex.printStackTrace();
								}
								Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + String.valueOf(currentPlaceLocation.getLatitude()) + "," + String.valueOf(currentPlaceLocation.getLongitude())));
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(intent);
							}
						})
						.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// do nothing
							}
						})
						.setIcon(android.R.drawable.ic_dialog_alert)
						.show();
				break;
			case R.id.previousHitButton:
				if (hitsnumber > 1) {
					Log.i("LOG", "HITS NUMBER " + hitsnumber);
					hitsnumber--;
					updateHitsCounter();
					//updateTextsCounter();
					IndexTable cIndex = hitsList.get(hitsnumber - 1);
					currentParagraphIndexNum = theParagraphsNumbers.indexOf("§" + cIndex.getPlocation());//mapIndex.get("§"+cIndex.getPlocation());
					scrollToSelectedHit();
				}
				break;
			case R.id.nextHitButton:
				if (hitsnumber < totalHits) {
					hitsnumber++;
					//calculateText();
					updateHitsCounter();
					//updateTextsCounter();
					IndexTable cIndex = hitsList.get(hitsnumber - 1);
					//currentParagraphIndexNum = theParagraphsNumbers.indexOf("§"+cIndex.getPlocation());//mapIndex.get("§"+cIndex.getPlocation());
					//scrollToSelectedHit();
				}
				break;
			case R.id.previousTextButton:
				if (currentTextIndex > 0) {
					currentTextIndex--;
					currentHitIndex = -1;
					hitsnumber = 1;
					updateTextsCounter();
					//calculateHits();
					//updateHitsCounter();
					updateView();
				}
				break;
			case R.id.nextTextButton:
				if (currentTextIndex + 1 < totalTexts) {
					currentTextIndex++;
					currentHitIndex = -1;
					hitsnumber = 1;
					updateTextsCounter();
					//calculateHits();
					//updateHitsCounter();
					updateView();
				}
				break;
			case internalLayout:
				break;
			case R.id.standard_map:
				myMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				break;
			case R.id.satellite_map:
				myMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
				break;
			case R.id.offline_map: {
				offlineOverlay = myMap.addTileOverlay(new TileOverlayOptions().tileProvider(new CustomMapTileProvider(getResources().getAssets())));
			}
			break;
			case R.id.online_map: {
				if (offlineOverlay != null)
					offlineOverlay.remove();
			}
			break;
			case R.id.myPositionButton:
				if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					// TODO: Consider calling
					//    ActivityCompat#requestPermissions
					// here to request the missing permissions, and then overriding
					//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
					//                                          int[] grantResults)
					// to handle the case where the user grants the permission. See the documentation
					// for ActivityCompat#requestPermissions for more details.
					return;
				}
				if (!myMap.isMyLocationEnabled()) {
					myLocButton.setBackgroundResource(R.drawable.corner_rectangle_dark_brown);
					myMap.setMyLocationEnabled(true);
					mGoogleApiClient.connect();
				} else {
					myMap.setMyLocationEnabled(false);
					mGoogleApiClient.disconnect();
					myLocButton.setBackgroundResource(R.drawable.corner_rectangle_dark_grey);
				}
            /*if (gps == null){
                gps = new GPSTracker(this);
            }

            if (myMap.isMyLocationEnabled()){
                myMap.setMyLocationEnabled(false);
                gps.stopUsingGPS();
                myLocButton.setBackgroundResource(R.drawable.corner_rectangle_dark_grey);
            }else{
                if (gps.canGetLocation()){
                    myMap.setMyLocationEnabled(true);
                    double lat = gps.getLatitude();
                    double lon = gps.getLongitude();
                    myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lon), 15));
                    myLocButton.setBackgroundResource(R.drawable.corner_rectangle_dark_brown);
                }else{
                    gps.showSettingsAlert();
                }
            }*/
            /*if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this,R.string.not_map_permissions,Toast.LENGTH_LONG).show();
                break;
            }else{
                theMap.setMyLocationEnabled(true);
                LocationManager locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
                Criteria crit = new Criteria();
                crit.setAccuracy(Criteria.ACCURACY_FINE);
                Location location = locationManager.getLastKnownLocation(locationManager
                        .getBestProvider(crit, false));
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                theMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), 6));

            }*/


            break;
        case R.id.sendFeedback:
            View pv = popupWindow.getContentView();
            EditText emailet = (EditText) pv.findViewById(R.id.feedback_email);
            EditText commentset = (EditText) pv.findViewById(R.id.feedback_comment);
            RatingBar ratingFeedback = (RatingBar) pv.findViewById(R.id.feedbackRating);
            if (commentset.getText().length() != 0 && emailet.getText().length() != 0 ){
                if (isValidEmail(emailet.getText())) {
                    new GetRequest(this).execute("http://topostext.org/rest/feedback.php?workID=" + getWorkId() + "&rating=" + ratingFeedback.getRating() + "&comments=" + commentset.getText() + "&email=" + emailet.getText());
                    popupWindow.dismiss();
                }else{
                    Toast.makeText(this,getResources().getString(R.string.not_valid_email),Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(this,getResources().getString(R.string.fill_all_fields),Toast.LENGTH_LONG).show();
            }

            break;
		}
	}
	
	private void updateView(){
		loadingView.setVisibility(View.VISIBLE);
		paragraphsListView.setAdapter(null);
		AsyncTask.execute(new Runnable() {
			   @Override
			   public void run() {
					theParagraphsList.clear();
					theGrParagraphsList.clear();
					theParagraphsNumbers.clear();
					mapIndex.clear();
					wdateList.clear();
					edateList.clear();
					
					IndexTable currentIndex = hitsList.get(currentHitIndex);//currentTextIndex);
					/*if (currentHitIndex >=0){
						HashMap<String,ArrayList<IndexTable>> myHashmap = (HashMap<String,ArrayList<IndexTable>>)getIntent().getExtras().getSerializable("hashmapList");
						ArrayList<IndexTable>  childs = myHashmap.get(currentIndex.getWorkTitle());
						currentIndex = childs.get(currentHitIndex);
					}*/
					
					final IndexTable finalCurrentIndex = currentIndex;
					workName = currentIndex.getWorkTitle();
					
					currentParagraphID = ""+currentIndex.getParagraph();
					getParagraphs(finalCurrentIndex.getWork_id());
					getIndexList(theParagraphsNumbers);
		
					runOnUiThread(new Runnable() {
			 	           @Override
			 	           public void run() { 
			 	        	  
						        workTitle.setText(workName);
						        ParagraphsListAdapter listAdapter = new ParagraphsListAdapter(FullWorkActivity.this, R.layout.paragraph_row,theParagraphsList,currentPlaceName,currentPlaceID);
								paragraphsListView.setAdapter(listAdapter);
								listAdapter.notifyDataSetChanged(); 
						        displayIndex();
								
								paragraphsListView.setSelection(currentParagraphIndexNum);
								if	(wdateList.size()>0 && edateList.size()>0){
						        	datesTitle.setText("Written ~"+wdateList.get(0)+", events ~"+edateList.get(0));
						        }
								
								loadingView.setVisibility(View.GONE);
			 	           }
					});
					
			   }
		});
		
		//IndexTable currentIndex = hitsList.get(currentTextIndex);
		//new MoreIndexTableLoading(this,database,this,null).execute(currentIndex);

	}
	
	public void scrollToSelectedHit(){
		paragraphsListView.setSelection(currentParagraphIndexNum);
	}
	
	public void showPopup(){
		LayoutInflater layoutInflater 
	     = (LayoutInflater)getBaseContext()
	      .getSystemService(LAYOUT_INFLATER_SERVICE);  
	    View popupView = layoutInflater.inflate(R.layout.activity_credits_popup, null);  
	             //final PopupWindow 
	    		popupWindow = new PopupWindow(
	               popupView, 
	               LayoutParams.MATCH_PARENT,  
	                     LayoutParams.MATCH_PARENT);

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            RelativeLayout bglayout = (RelativeLayout) popupView.findViewById(R.id.clickableBackground);
            bglayout.setOnClickListener(this);
        }else{
            LinearLayout bglayout = (LinearLayout) popupView.findViewById(R.id.clickableBackground);
            bglayout.setOnClickListener(this);
        }
	    LinearLayout internalLayout = (LinearLayout) popupView.findViewById(R.id.internalLayout);

	    internalLayout.setFocusableInTouchMode(false);
	    internalLayout.setClickable(false);

	    //internalLayout.setOnClickListener(this);
	    TextView creditsText = (TextView) popupView.findViewById(R.id.creditsText);
	    //creditsText.setText(getWorkCredits());
	    creditsText.setText(noTrailingwhiteLines(Html.fromHtml(getWorkCredits())));
	    creditsText.setMovementMethod(LinkMovementMethod.getInstance());
	     popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
	}

    private void showFeedbackpopup(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.feedback_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

		popupWindow.setFocusable(true);
		popupWindow.update();
        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);

        TextView currentWorkName = (TextView) popupView.findViewById(R.id.dynamic_name);
        currentWorkName.setText(workTitle.getText());

        Button sendFeedbackButton = (Button) popupView.findViewById(R.id.sendFeedback);
        sendFeedbackButton.setOnClickListener(this);

        clickedBG.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }
	
	private CharSequence noTrailingwhiteLines(CharSequence text) {

	    while (text.charAt(text.length() - 1) == '\n') {
	        text = text.subSequence(0, text.length() - 1);
	    }
	    return text;
	}

	@Override
	public void indexReady(ArrayList<IndexTable> theList) {
		// TODO Auto-generated method stub
		hitsList = theList;
		hitsnumber=1;
		totalHits = theList.size();
		updateHitsCounter();
	}

	@Override
	public void expandableIndexReady(ArrayList<IndexTable> theList,
			HashMap<String, ArrayList<IndexTable>> theHashmap) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(@Nullable Bundle bundle) {
		Log.i("LOG","ON CONNECTED CALLED");
		if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
		Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
				mGoogleApiClient);

		myMap.setMyLocationEnabled(true);

		myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()), 15));
		myLocButton.setBackgroundResource(R.drawable.corner_rectangle_dark_brown);
	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

	}
}
