package com.pavla.topostext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.ClusteringSettings;
import com.androidmapsextensions.OnMapReadyCallback;
import com.androidmapsextensions.SupportMapFragment;
import com.androidmapsextensions.TileOverlay;
import com.androidmapsextensions.TileOverlayOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;

import com.androidmapsextensions.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.android.gms.maps.model.TileOverlayOptions;
//import com.google.maps.android.clustering.ClusterManager;
//import com.google.maps.android.clustering.algo.Algorithm;
//import com.google.maps.android.clustering.algo.GridBasedAlgorithm;
//import com.google.maps.android.clustering.algo.PreCachingAlgorithmDecorator;
import com.pavla.ToposTextDatabase.ExternalDbOpenHelper;
import com.pavla.topostextHelpers.CustomMapTileProvider;
import com.pavla.topostextHelpers.DemoFragment;
import com.pavla.topostextHelpers.GooglePlayServicesErrorDialogFragment;
import com.pavla.topostextHelpers.IconsKeyListAdapter;
import com.pavla.topostextHelpers.MyListener;
import com.pavla.topostextHelpers.Person;


import com.androidmapsextensions.Circle;
import com.androidmapsextensions.CircleOptions;
import com.androidmapsextensions.ClusteringSettings;
import com.androidmapsextensions.GoogleMap.InfoWindowAdapter;
import com.androidmapsextensions.GoogleMap.OnInfoWindowClickListener;
import com.androidmapsextensions.GoogleMap.OnMapClickListener;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;

import info.hoang8f.android.segmented.SegmentedGroup;

import static android.R.attr.enabled;
import static android.R.attr.icon;
import static android.R.attr.name;
import static android.R.attr.text;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;
import static java.lang.Boolean.FALSE;


public class MapActivity extends AppCompatActivity implements OnClickListener, MyListener, OnInfoWindowClickListener, OnMapReadyCallback, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private static final String DB_NAME = "ToposTextDatabaseAndroid.sqlite";
    public static final String MY_PREFS_NAME = "ToposPrefs";

    //A good practice is to define field names as constants
    private static final String TABLE_NAME = "ZPLACES";
    private static final String TEXTS_TABLE_NAME = "ZWORKS";
    private static final String PEOPLE_TABLE_NAME = "ZPERSON";
    private static final String WORK_NAME = "ZTITLE";
    private static final String PERSON_NAME = "ZNAME";
    private static final String PLACE_ID = "_id";
    private static final String LATITUDE = "ZLATITUDE";
    private static final String LONGITUDE = "ZLONGITUDE";
    private static final String ICON = "ZICON_NAME";
    private static final String PLACE_NAME = "ZDISPLAYNAME";
    private static final String PLACE_DESCRIPTION = "ZDESCR";
    private static final String PRIORITY = "ZPRIORITY";
    private static final LatLng latlng = new LatLng(37.9383358, 23.6467971);
    private SQLiteDatabase database;
    private Fragment currentFragment;
    private ImageButton infoButton;
    private PopupWindow popupWindow, iconsKeyPopup, popupWindowFirstTime;
    private ScrollView firstScrollView, secondScrollView;
    ArrayList<String> thePlacesList = new ArrayList<String>();
    ArrayList<String> theWorksList = new ArrayList<String>();
    ArrayList<String> thePeopleList = new ArrayList<String>();
    List<Marker> allMarkers = new ArrayList<Marker>();
    private ProgressBar circleView;
    //private RelativeLayout loadingView;
    private TextView backgroundView;
    private Marker newMarker;
    private Marker clickedMarker;
    private RelativeLayout mapLayout;
    private Button filtersButton;
    private RadioButton offlineMap;

    private SearchView searchView;
    private Boolean FIRST_MENU = true;

    private TileOverlay offlineOverlay;
    private ImageButton placesButton, textsButton, peopleButton, myLocationButton;


    private PopupWindow loadingPopupWindow;

    //private ClusterManager<Person> mClusterManager;

    HashMap<String, Marker> markersList = new HashMap<String, Marker>();
    HashMap<String, MarkerOptions> visibleMarkersOptionsList = new HashMap<String, MarkerOptions>();
    HashMap<String, MarkerOptions> allMarkerOptions = new HashMap<String, MarkerOptions>();
    HashMap<String, String> iconsMarkers = new HashMap<String, String>();

    ArrayList<Marker> priority1Markers = new ArrayList<Marker>();
    ArrayList<Marker> priority2Markers = new ArrayList<Marker>();
    ArrayList<Marker> priority3Markers = new ArrayList<Marker>();
    ArrayList<Marker> priority4Markers = new ArrayList<Marker>();
    ArrayList<Marker> priority5Markers = new ArrayList<Marker>();
    ArrayList<Marker> priority6Markers = new ArrayList<Marker>();

    ArrayList<MarkerOptions> priority1MarkersOptions = new ArrayList<MarkerOptions>();
    ArrayList<MarkerOptions> priority2MarkersOptions = new ArrayList<MarkerOptions>();
    ArrayList<MarkerOptions> priority3MarkersOptions = new ArrayList<MarkerOptions>();
    ArrayList<MarkerOptions> priority4MarkersOptions = new ArrayList<MarkerOptions>();
    ArrayList<MarkerOptions> priority5MarkersOptions = new ArrayList<MarkerOptions>();
    ArrayList<MarkerOptions> priority6MarkersOptions = new ArrayList<MarkerOptions>();

    GoogleMap theMap;

    private SupportMapFragment mapFragment;
    private ListView icons_images_names;

    //Initialize to a non-valid zoom value
    private float previousZoomLevel = -1.0f;
    private boolean isZooming = false;


    private static final int PLACES = 0;
    private static final int WORKS = 1;
    private static final int PEOPLE = 2;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            getSupportActionBar().hide();
        } else {
            getSupportActionBar().setIcon(R.drawable.logo_laskaridis_s);
            getSupportActionBar().setLogo(R.drawable.logo_laskaridis_s);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().show();
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        }




        setContentView(R.layout.activity_map);
		
		/*MapFragment mapFragment = (MapFragment) getFragmentManager()
			    .findFragmentById(R.id.map);
			mapFragment.getMapAsync(this);*/

        /*if (savedInstanceState == null) {
            if (GooglePlayServicesErrorDialogFragment.showDialogIfNotAvailable(this)) {
                //replaceMainFragment(new DemoFragment());
            }
        }*/

        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            mapLayout = (RelativeLayout) findViewById(R.id.maplayout);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }


        int checkGooglePlayServices = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
            // google play services is missing!!!!
	    /* Returns status code indicating whether there was an error. 
	    Can be one of following in ConnectionResult: SUCCESS, SERVICE_MISSING, SERVICE_VERSION_UPDATE_REQUIRED, SERVICE_DISABLED, SERVICE_INVALID.
	    */
            //GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices, mActivity, 1122).show();
        }

        checkMapLoad();


        Typeface mycustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/NotoSerif-Regular.ttf");
        //Our key helper
        ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(this, DB_NAME);
        database = dbOpenHelper.openDataBase();
        //Button placesTab = (Button) findViewById(R.id.placesTab);
        //Button textsTab = (Button) findViewById(R.id.textsTab);

        RadioButton placesRadio = (RadioButton) findViewById(R.id.placesRadio);
        RadioButton textsRadio = (RadioButton) findViewById(R.id.textsRadio);
        RadioButton onlineMap = (RadioButton) findViewById(R.id.online_map);
        offlineMap = (RadioButton) findViewById(R.id.offline_map);
        RadioButton standardMap = (RadioButton) findViewById(R.id.standard_map);
        RadioButton satelliteMap = (RadioButton) findViewById(R.id.satellite_map);
        searchView = (SearchView) findViewById(R.id.mySearchView);
        filtersButton = (Button) findViewById(R.id.filter_button);
        icons_images_names = (ListView) findViewById(R.id.filtersListview);

        searchView.setFocusable(true);
        searchView.setFocusable(false);
        searchView.setIconified(false);
        searchView.clearFocus();
        searchView.setOnSearchClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //HIDE KEYBOARD
                InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });

        placesButton = (ImageButton) findViewById(R.id.placesButton);
        textsButton = (ImageButton) findViewById(R.id.textsButton);
        peopleButton = (ImageButton) findViewById(R.id.peopleButton);
        myLocationButton = (ImageButton) findViewById(R.id.myLocationButton);


        infoButton = (ImageButton) findViewById(R.id.infobutton);
        placesRadio.setTypeface(mycustomFont);
        textsRadio.setTypeface(mycustomFont);
        circleView = (ProgressBar) findViewById(R.id.circleView);
        //loadingView = (RelativeLayout) findViewById(R.id.loadingView);
        //backgroundView = (TextView) findViewById(R.id.background);
        placesButton.setSelected(true);

        //placesTab.setOnClickListener(this);
        //textsTab.setOnClickListener(this);
        placesRadio.setOnClickListener(this);
        textsRadio.setOnClickListener(this);
        infoButton.setOnClickListener(this);
        onlineMap.setOnClickListener(this);
        offlineMap.setOnClickListener(this);
        standardMap.setOnClickListener(this);
        satelliteMap.setOnClickListener(this);
        placesButton.setOnClickListener(this);
        textsButton.setOnClickListener(this);
        peopleButton.setOnClickListener(this);
        filtersButton.setOnClickListener(this);
        myLocationButton.setOnClickListener(this);


        //readPlaces();
        readTexts();
        readPeople();

        //showHideCircleView(false);

        //database.close();

        addFragment(PLACES);
        //replaceMainFragment(new DemoFragment());
        FragmentManager fm = getSupportFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.main_container);
        if (mapFragment == null) {
            mapFragment = createMapFragment();
            FragmentTransaction tx = fm.beginTransaction();
            tx.add(R.id.main_container, mapFragment);
            tx.commit();
        }
        mapFragment.getExtendedMapAsync(this);

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }


    private void replaceMainFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tx = fm.beginTransaction();
        tx.replace(R.id.main_container, fragment);
        tx.commit();
    }

    private void createMapFragmentIfNeeded() {
        FragmentManager fm = getSupportFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (mapFragment == null) {
            mapFragment = createMapFragment();
            FragmentTransaction tx = fm.beginTransaction();
            tx.add(R.id.map_container, mapFragment);
            tx.commit();
        }
    }

    protected SupportMapFragment createMapFragment() {
        return SupportMapFragment.newInstance();
    }

    public void onStart() {
        super.onStart();
        //readPlaces();
        //readTexts();


    }

    public void onDestroy() {
        super.onDestroy();
    }


    public void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString("firstTimeLaunch", null);
        if (restoredText == null) {
            findViewById(R.id.LinearLayout1).post(new Runnable() {
                public void run() {
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("firstTimeLaunch", "yes");
                    editor.apply();
                    showFirstTimePopup();
                }
            });
        }

        //setUpMapIfNeeded();
    }


    protected void onStop() {
        mGoogleApiClient.disconnect();
        if (database.isOpen())  database.close();
        super.onStop();
    }


    protected void setUpMap() {
        if (theMap == null) {
            return;
        }

        if (!isNetworkOnline()) {
            offlineOverlay = theMap.addTileOverlay(new TileOverlayOptions().tileProvider(new CustomMapTileProvider(getResources().getAssets())));
            offlineMap.setChecked(true);
        }

        theMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 6));
        theMap.setOnInfoWindowClickListener(this);
        Log.i("LOG", "SETTING UP MAP!");
        ClusteringSettings clusteringSettings = new ClusteringSettings();
        clusteringSettings.addMarkersDynamically(true);

        clusteringSettings.clusterOptionsProvider(new DemoClusterOptionsProvider(getResources()));

        double clusterSize = 160;
        clusteringSettings.clusterSize(clusterSize);

        theMap.setClustering(clusteringSettings);
        //if (loadingPopupWindow == null)
        //	createLoadingPopup();
        //AsyncTask.execute(new Runnable() {
        //	@Override
        //	public void run() {
        //TODO your background code
        readPlacesForMap3();
        //	}
        //});

        BitmapDescriptor icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);

        theMap.getUiSettings().setMyLocationButtonEnabled(false);

    }


    public void checkMapLoad() {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString("mapLoaded", null);
        if (restoredText == null) {
            if (!isNetworkOnline()) {
                new AlertDialog.Builder(this)
                        .setMessage(getResources().getString(R.string.first_time_no_network))
                        .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }
        //if	()
    }

    // method invoke when mybutton will click
    public void clickedPlace(String placeName) {

        Marker clickedMarker = markersList.get(placeName);
        clickedMarker.showInfoWindow();
        if (clickedMarker != null) {
            CameraUpdate center =
                    CameraUpdateFactory.newLatLngZoom(new LatLng(clickedMarker.getPosition().latitude,
                            clickedMarker.getPosition().longitude), 19.0f);
            theMap.moveCamera(center);

        }

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            FIRST_MENU = true;
            mapLayout.setVisibility(View.VISIBLE);
            invalidateOptionsMenu();
        }

    }

    private void readPlaces() {
        // TODO Auto-generated method stub

        //routes = new ArrayList<String>();
        Cursor routesCursor = database.query(TABLE_NAME, new String[]{PLACE_ID,
                PLACE_NAME, PLACE_DESCRIPTION}, null, null, null, null, PLACE_NAME);
        routesCursor.moveToFirst();
        if (!routesCursor.isAfterLast()) {
            do {
                String id = routesCursor.getString(0);
                String name = routesCursor.getString(1);
                String description = routesCursor.getString(2);

                //String image_name = routesCursor.getString(5);
                thePlacesList.add(name);

            } while (routesCursor.moveToNext());
        }
        routesCursor.close();


    }


    private void readPlacesForMap2() {
        //routes = new ArrayList<String>();
        Cursor routesCursor = database.query(TABLE_NAME, new String[]{
                PLACE_NAME, LATITUDE, LONGITUDE, ICON, PRIORITY}, null, null, null, null, PLACE_NAME);
        routesCursor.moveToFirst();
        if (!routesCursor.isAfterLast()) {

            do {
                Log.i("LOG", "roUTE ITEM");
                final String name = routesCursor.getString(0);
                final float latitude = routesCursor.getFloat(1);
                final float longitude = routesCursor.getFloat(2);
                String icon_name = routesCursor.getString(3);
                int priority = routesCursor.getInt(4);
                icon_name = icon_name.replace(".png", "_ipad");
                icon_name = icon_name.replace("?", "q");
                Person offsetItem = new Person(new LatLng(latitude, longitude), "Walter", R.drawable.cemetery_ipad);
                //mClusterManager.addItem(offsetItem);


            } while (routesCursor.moveToNext());
        }
        routesCursor.close();
    }


    private void readPlacesForMap3() {
        //routes = new ArrayList<String>();
        showLoadingPopup();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                final Cursor routesCursor = database.query(TABLE_NAME, new String[]{
                        PLACE_NAME, LATITUDE, LONGITUDE, ICON, PRIORITY}, null, null, null, null, PLACE_NAME);
                routesCursor.moveToFirst();
                List<LatLng> result = new ArrayList<>();
                if (!routesCursor.isAfterLast()) {
                    thePlacesList.clear();
                    final MarkerOptions options = new MarkerOptions();
                    int i = 0;
                    do {
                        final String name = routesCursor.getString(0);
                        final float latitude = routesCursor.getFloat(1);
                        final float longitude = routesCursor.getFloat(2);
                        String icon_name = routesCursor.getString(3);
                        int priority = routesCursor.getInt(4);
                        icon_name = icon_name.replace(".png", "_ipad");
                        icon_name = icon_name.replace("?", "q");
                        result.add(new LatLng(latitude, longitude));
                        final int turn = i;

                        final String ic_name = icon_name;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //Your code to run in GUI thread here
                                Marker themarker;
                                if (!ic_name.equals("")) {
                                    final BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(getDrawableResourceIdByName(ic_name));
                                    themarker = theMap.addMarker(options.title(name).position(new LatLng(latitude, longitude)).icon(icon));
                                } else {
                                    themarker = theMap.addMarker(options.title(name).position(new LatLng(latitude, longitude)));
                                }
                                thePlacesList.add(name);
                                markersList.put(name, themarker);

                                if (turn == routesCursor.getCount() - 1) {
                                    if (loadingPopupWindow != null)
                                        loadingPopupWindow.dismiss();
                                    if (currentFragment.getClass() == PlacesListFragment.class) {
                                        PlacesListFragment fr = (PlacesListFragment) currentFragment;
                                        Log.i("LOG", "list updated with valuessss : " + thePlacesList);
                                        fr.updateListAdapter(thePlacesList);
                                    }
                                }
                            }//public void run() {
                        });


                        i++;

                    } while (routesCursor.moveToNext());
                }
                routesCursor.close();
            }

        });

    }

    private void readPlacesForMapWithIconName(String iconName) {
        //routes = new ArrayList<String>();
        //Cursor routesCursor = database.query(TABLE_NAME, new String[] {
        //		PLACE_NAME,LATITUDE,LONGITUDE,ICON,PRIORITY}, null, null, null, null, PLACE_NAME);
        Cursor routesCursor = database.query(TABLE_NAME, new String[]{PLACE_NAME, LATITUDE, LONGITUDE, ICON, PRIORITY}, ICON + " = '" + iconName + ".png'", null, null, null, PLACE_NAME);
        routesCursor.moveToFirst();
        List<LatLng> result = new ArrayList<>();
        if (!routesCursor.isAfterLast()) {
            thePlacesList.clear();
            MarkerOptions options = new MarkerOptions();
            do {
                final String name = routesCursor.getString(0);
                final float latitude = routesCursor.getFloat(1);
                final float longitude = routesCursor.getFloat(2);
                String icon_name = routesCursor.getString(3);
                int priority = routesCursor.getInt(4);
                icon_name = icon_name.replace(".png", "_ipad");
                icon_name = icon_name.replace("?", "q");
                result.add(new LatLng(latitude, longitude));
                Log.i("LOG", "should add something");
                Marker themarker;
                if (!icon_name.equals("")) {
                    final BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(getDrawableResourceIdByName(icon_name));
                    theMap.addMarker(options.title(name).position(new LatLng(latitude, longitude)).icon(icon));
                } else {
                    theMap.addMarker(options.title(name).position(new LatLng(latitude, longitude)));
                }
                thePlacesList.add(name);

            } while (routesCursor.moveToNext());

            if (currentFragment.getClass() == PlacesListFragment.class) {
                PlacesListFragment fr = (PlacesListFragment) currentFragment;
                Log.i("LOG", "list updated with valuess : " + thePlacesList);
                fr.updateListAdapter(thePlacesList);
            }
        }
        routesCursor.close();

    }


    private int getDrawableResourceIdByName(String icon_name) {
        Resources resources = this.getResources();
        final int resourceId = resources.getIdentifier(icon_name, "drawable",
                this.getPackageName());
        return resourceId;

    }


    private void readTexts() {
        // TODO Auto-generated method stub

        //routes = new ArrayList<String>();
        Cursor routesCursor = database.query(TEXTS_TABLE_NAME, new String[]{WORK_NAME}, null, null, null, null, WORK_NAME);
        routesCursor.moveToFirst();
        if (!routesCursor.isAfterLast()) {
            do {
                String workTitle = routesCursor.getString(0);

                theWorksList.add(workTitle);

            } while (routesCursor.moveToNext());
        }
        routesCursor.close();

    }

    private void readPeople() {
        // TODO Auto-generated method stub

        //routes = new ArrayList<String>();
        Cursor routesCursor = database.query(PEOPLE_TABLE_NAME, new String[]{PERSON_NAME}, null, null, null, null, PERSON_NAME);
        routesCursor.moveToFirst();
        if (!routesCursor.isAfterLast()) {
            do {
                String personName = routesCursor.getString(0);

                thePeopleList.add(personName);

            } while (routesCursor.moveToNext());
        }
        routesCursor.close();

    }


    private void addFragment(int fragmentID) {

        Fragment newFragment;

        switch (fragmentID) {
            case PLACES: {
                PlacesListFragment mynewFragment = new PlacesListFragment();
                mynewFragment.placesList = thePlacesList;
                newFragment = mynewFragment;
                //Bundle b = new Bundle();
                //b.putSerializable("places", thePlacesList);
                //newFragment.setArguments(b);

                break;
            }
            case WORKS: {
                newFragment = new WorksListFragment();
                Bundle b = new Bundle();
                b.putSerializable("texts", theWorksList);
                newFragment.setArguments(b);
                break;
            }
            case PEOPLE: {
                PeopleListFragment mynewFragment = new PeopleListFragment();
                mynewFragment.peopleList = thePeopleList;
                newFragment = mynewFragment;
                //Bundle b = new Bundle();
                //b.putSerializable("people", thePeopleList);
                //newFragment.setArguments(b);
                break;
            }

            default:
                newFragment = new PlacesListFragment();
                break;
        }
        // Create new fragment and transaction
        currentFragment = newFragment;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        transaction.replace(R.id.rightMapList, newFragment);


        // Commit the transaction
        transaction.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (FIRST_MENU)
            getMenuInflater().inflate(R.menu.map, menu);
        else
            getMenuInflater().inflate(R.menu.map_second, menu);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (FIRST_MENU)
            getMenuInflater().inflate(R.menu.map, menu);
        else
            getMenuInflater().inflate(R.menu.map_second, menu);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.lista) {
            FIRST_MENU = false;
            mapLayout.setVisibility(View.GONE);
            invalidateOptionsMenu();
            return true;
        } else if (id == R.id.map) {
            FIRST_MENU = true;
            mapLayout.setVisibility(View.VISIBLE);
            invalidateOptionsMenu();
        }
        return super.onOptionsItemSelected(item);
    }

	/*@Override
	public void onMapReady(GoogleMap map) {
		SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE); 
		String restoredText = prefs.getString("mapLoaded", null);
		if (restoredText == null) {
			SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
			 editor.putString("mapLoaded", "yes");
			 editor.apply();
		}
		
		
		
8
		map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
		if	(!isNetworkOnline())
			map.addTileOverlay(new TileOverlayOptions().tileProvider(new CustomMapTileProvider(getResources().getAssets())));
		
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 6));
		
		theMap = map;
		
		mClusterManager = new ClusterManager<Person>(this, theMap);
		//GridBasedAlgorithm<Person> gridAlgorithm = new GridBasedAlgorithm<Person>();
		//mClusterManager.setAlgorithm(new PreCachingAlgorithmDecorator<Person>(gridAlgorithm));
       // mClusterManager.setRenderer(new PersonRenderer());
		
		theMap.setOnInfoWindowClickListener(this);
		theMap.setOnMarkerClickListener(this);
		theMap.setOnCameraIdleListener(mClusterManager);
		theMap.setOnCameraChangeListener(getCameraChangeListener());
		AsyncTask.execute(new Runnable() {
		   @Override
		   public void run() {
		      //TODO your background code
			   readPlacesForMap2();
		   }
		});

		if (map == null) {
			return;
		}
		ClusteringSettings clusteringSettings = new ClusteringSettings();
		clusteringSettings.addMarkersDynamically(true);

		clusteringSettings.clusterOptionsProvider(new DemoClusterOptionsProvider(getResources()));

		double clusterSize = 160;
		clusteringSettings.clusterSize(clusterSize);

		theMap.setClustering(clusteringSettings);

		AsyncTask.execute(new Runnable() {
		   @Override
		   public void run() {
		      //TODO your background code
			   readPlacesForMap3();
		   }
		});

		BitmapDescriptor icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
		
		
		
		//readPlacesForMap();
		//database.close();
	}*/

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            showExitDialog();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
        showExitDialog();
    }

    private void showExitDialog(){
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.exit_title))
                .setMessage(getResources().getString(R.string.exit_message))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }


    public boolean isNetworkOnline() {
        boolean status = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = cm.getNetworkInfo(1);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED)
                    status = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return status;

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.placesButton:
                textsButton.setSelected(false);
                peopleButton.setSelected(false);
                placesButton.setSelected(true);
            case R.id.placesRadio:
                addFragment(PLACES);
                break;
            case R.id.textsButton:
                placesButton.setSelected(false);
                peopleButton.setSelected(false);
                textsButton.setSelected(true);
            case R.id.textsRadio:
                addFragment(WORKS);
                break;
            case R.id.infobutton:
                showInfo();
                break;
            case R.id.clickableBackground:
                if (popupWindow != null)
                    popupWindow.dismiss();
                if (iconsKeyPopup != null)
                    iconsKeyPopup.dismiss();
                break;
            case R.id.internalLayout:
                break;
            case R.id.ephoriaAddressesButton:
                firstScrollView.setVisibility(View.GONE);
                secondScrollView.setVisibility(View.VISIBLE);
                break;
            case R.id.backButton:
                secondScrollView.setVisibility(View.GONE);
                firstScrollView.setVisibility(View.VISIBLE);
                break;
            case R.id.hide_button:
                popupWindowFirstTime.dismiss();
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("firstTimeLaunch", "yes");
                editor.apply();
                popupWindowFirstTime.dismiss();
                break;
            case R.id.standard_map:
                theMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.satellite_map:
                theMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                if (offlineMap.isChecked())
                Toast.makeText(this,getResources().getString(R.string.satellite_note),Toast.LENGTH_LONG).show();
                break;
            case R.id.peopleButton:
                textsButton.setSelected(false);
                placesButton.setSelected(false);
                peopleButton.setSelected(true);
                addFragment(PEOPLE);
                break;
            case R.id.filter_button:
                //if (icons_images_names.getVisibility() == View.GONE)
                showIconsKey();
                //else
                //	icons_images_names.setVisibility(View.GONE);
            case R.id.offline_map:
                offlineOverlay = theMap.addTileOverlay(new TileOverlayOptions().tileProvider(new CustomMapTileProvider(getResources().getAssets())));
                break;
            case R.id.online_map:
                offlineOverlay.remove();
                break;
            case R.id.myLocationButton:
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    new AlertDialog.Builder(this)
                            .setTitle("Location permission")
                            .setMessage("We have no permission to access your location. Would you like to change it?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    return;
                }
                if (!theMap.isMyLocationEnabled()) {
                    myLocationButton.setBackgroundResource(R.drawable.corner_rectangle_dark_brown);
                    theMap.setMyLocationEnabled(true);
                    mGoogleApiClient.connect();
                } else {
                    theMap.setMyLocationEnabled(false);
                    mGoogleApiClient.disconnect();
                    myLocationButton.setBackgroundResource(R.drawable.corner_rectangle_dark_grey);
                }
				/*if (gps == null){
					gps = new GPSTracker(this);
				}

				if (theMap.isMyLocationEnabled()){
					theMap.setMyLocationEnabled(false);
					gps.stopUsingGPS();
                    myLocationButton.setBackgroundResource(R.drawable.corner_rectangle_dark_grey);
				}else{
					if (gps.canGetLocation()){
						theMap.setMyLocationEnabled(true);
						gps.getLocation();
						double lat = gps.getLatitude();
						double lon = gps.getLongitude();
						theMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lon), 15));
                        myLocationButton.setBackgroundResource(R.drawable.corner_rectangle_dark_brown);


					}else{
						gps.showSettingsAlert();
					}
				}*/

				/*if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					Toast.makeText(this,R.string.not_map_permissions,Toast.LENGTH_LONG).show();
					break;
				}else{
					theMap.setMyLocationEnabled(true);
					LocationManager locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
					Criteria crit = new Criteria();
					crit.setAccuracy(Criteria.ACCURACY_FINE);
					Location location = locationManager.getLastKnownLocation(locationManager
                            .getBestProvider(crit, false));
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
					theMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), 6));

				}*/


                break;
            default:
                break;

        }
    }

    public void showInfo() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.info_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);
	             
	             /*Button btnDismiss = (Button)popupView.findViewById(R.id.dismiss);
	             btnDismiss.setOnClickListener(new Button.OnClickListener(){

	     @Override
	     public void onClick(View v) {
	      // TODO Auto-generated method stub
	      popupWindow.dismiss();
	     }});*/
        LinearLayout bglayout = (LinearLayout) popupView.findViewById(R.id.clickableBackground);
        LinearLayout internallayout = (LinearLayout) popupView.findViewById(R.id.internalLayout);
        bglayout.setOnClickListener(this);
        internallayout.setOnClickListener(this);

        firstScrollView = (ScrollView) popupView.findViewById(R.id.firstScrollView);
        secondScrollView = (ScrollView) popupView.findViewById(R.id.secondScrollView);
        secondScrollView.setVisibility(View.GONE);

        //Typeface mycustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/NotoSerif-Regular.ttf");
        TextView infoText = (TextView) popupView.findViewById(R.id.infotext);
        TextView infoLinks = (TextView) popupView.findViewById(R.id.infolinks);
        //infoText.setTypeface(mycustomFont);
        //infoLinks.setTypeface(mycustomFont);
        infoText.setText(Html.fromHtml(getString(R.string.info_text)));
        infoLinks.setText(Html.fromHtml(getString(R.string.info_links)));
        infoLinks.setMovementMethod(LinkMovementMethod.getInstance());

        Button ephoriaAddressesButton = (Button) popupView.findViewById(R.id.ephoriaAddressesButton);
        Button backButton = (Button) popupView.findViewById(R.id.backButton);
        ephoriaAddressesButton.setOnClickListener(this);
        backButton.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 15, 15);

    }


    public void showFirstTimePopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.first_time_popup, null);
        //final PopupWindow
        popupWindowFirstTime = new PopupWindow(
                popupView,
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);

        Typeface mycustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/NotoSerif-Regular.ttf");
        Button btnDismiss = (Button) popupView.findViewById(R.id.hide_button);
        btnDismiss.setTypeface(mycustomFont, Typeface.BOLD);
        btnDismiss.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupWindowFirstTime.dismiss();

            }
        });
        //Typeface mycustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/NotoSerif-Regular.ttf");
        //Button hideButton = (Button) popupView.findViewById(R.id.hide_button);
        //hideButton.setTypeface(mycustomFont,Typeface.BOLD);
        //hideButton.setOnClickListener(this);

        TextView firstTimeText = (TextView) popupView.findViewById(R.id.firsttimetext);
        firstTimeText.setTypeface(mycustomFont);
	    
	      /*LinearLayout bglayout = (LinearLayout) popupView.findViewById(R.id.firstTimeLayout);
	      LinearLayout internallayout = (LinearLayout) popupView.findViewById(R.id.internalLayout);
	      bglayout.setOnClickListener(this);
	      internallayout.setOnClickListener(this);
	      
	      firstScrollView = (ScrollView) popupView.findViewById(R.id.firstScrollView);
	      secondScrollView = (ScrollView) popupView.findViewById(R.id.secondScrollView);
	      secondScrollView.setVisibility(View.GONE);
	      
	      TextView infoText = (TextView) popupView.findViewById(R.id.infotext);
	      TextView infoLinks = (TextView) popupView.findViewById(R.id.infolinks);
	      infoText.setText(Html.fromHtml(getString(R.string.info_text)));
	      infoLinks.setText(Html.fromHtml(getString(R.string.info_links)));
	      
	      Button ephoriaAddressesButton = (Button) popupView.findViewById(R.id.ephoriaAddressesButton);
	      Button backButton = (Button) popupView.findViewById(R.id.backButton);
	      ephoriaAddressesButton.setOnClickListener(this);
	      backButton.setOnClickListener(this);*/

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindowFirstTime.showAtLocation(findViewById(android.R.id.content), Gravity.TOP, 0, 0);

    }

    private void createLoadingPopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.loading_popupview, null);
        //final PopupWindow
        loadingPopupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                getPopupHeight());
    }

    private int getPopupHeight() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Display display = this.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            int statusBarHeight = 0;
            int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = getResources().getDimensionPixelSize(resourceId);
            }
            return size.y + 2*statusBarHeight;
        } else {
            return ViewGroup.LayoutParams.MATCH_PARENT;
        }
    }

    private void showLoadingPopup() {

        createLoadingPopup();
        //popupWindow.showAsDropDown(infoButton, 50, -30);
        //loadingPopupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);

        findViewById(android.R.id.content).post(new Runnable() {
            public void run() {
                loadingPopupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
            }
        });
    }


    public void showIconsKey() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.icons_key_popup, null);
        //final PopupWindow
        iconsKeyPopup = new PopupWindow(
                popupView,
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);

        LinearLayout bglayout = (LinearLayout) popupView.findViewById(R.id.clickableBackground);
        LinearLayout internallayout = (LinearLayout) popupView.findViewById(R.id.internalLayout);
        bglayout.setOnClickListener(this);
        internallayout.setOnClickListener(this);

        ListView list = (ListView) popupView.findViewById(R.id.keysList);

        ArrayList<String> iconnames = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.iconNames)));
        ArrayList<String> imagenames = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.imagesNames)));
        IconsKeyListAdapter listAdapter = new IconsKeyListAdapter(this, R.layout.key_row, iconnames, imagenames);
        //icons_images_names.setAdapter(listAdapter);
        list.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();

        //icons_images_names.setVisibility(View.VISIBLE);
        list.setOnItemClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        iconsKeyPopup.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);

    }

    private void hideMarker(Marker theMarkerToHide) {
        if (visibleMarkersOptionsList.containsKey(theMarkerToHide.getTitle()))
            theMarkerToHide.setVisible(false);
    }
	
	/*private void showHideCircleView(boolean show){
		if	(show){
			loadingView.setVisibility(View.VISIBLE);
        	//backgroundView.setVisibility(View.VISIBLE);
		}else{
			loadingView.setVisibility(View.INVISIBLE);
        	//backgroundView.setVisibility(View.INVISIBLE);
		}
	}*/

    @Override
    public void onInfoWindowClick(Marker marker) {
        // TODO Auto-generated method stub
        Intent intent = new Intent(this, IndexTableActivity.class);
        String cleanTitle = marker.getTitle().replace("'","''");
        //intent.putExtra("placename", cleanTitle);
        IndexTableActivity.placeName = cleanTitle;
        startActivity(intent);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString("mapLoaded", null);
        if (restoredText == null) {
            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("mapLoaded", "yes");
            editor.apply();
        }

        Log.i("LOG", "should set up map here! ");
        theMap = googleMap;
        if (theMap != null) {
            setUpMap();
			/*if (currentFragment.getClass() == PlacesListFragment.class) {
				PlacesListFragment fr = (PlacesListFragment) currentFragment;
				fr.updateListAdapter(thePlacesList);
			}*/
        }
    }

    public SearchView getSearchView() {
        return searchView;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this, "Clicked somethign", Toast.LENGTH_LONG).show();
        iconsKeyPopup.dismiss();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //if (position == 0)
        //	showLoadingPopup();
        IconsKeyListAdapter myAdapter = (IconsKeyListAdapter) parent.getAdapter();
        ArrayList<String> iconsarray = myAdapter.getIcons();
        String icon_name = iconsarray.get(position);
        String icon_name_stripped = icon_name.replace("_ipad", "");
        icon_name_stripped = icon_name_stripped.replace("q", "?");


        //FILTER PLACES BASED ON THAT ITEM
        //UPDATE MAP
        theMap.clear();
        iconsKeyPopup.dismiss();
        if (position == 0) {
            //loadingPopupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
            readPlacesForMap3();

        } else
            readPlacesForMapWithIconName(icon_name_stripped);

        //change button name
        TextView firstInfo = (TextView) view.findViewById(R.id.firsttitle);
        filtersButton.setText(firstInfo.getText());

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i("LOG","ON CONNECTED CALLED");
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        theMap.setMyLocationEnabled(true);

        theMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()), 15));
        myLocationButton.setBackgroundResource(R.drawable.corner_rectangle_dark_brown);
    }

	@Override
	public void onConnectionSuspended(int i) {

	}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("LOG","ON CONNECTION FAILED CALLED");
    }
}
