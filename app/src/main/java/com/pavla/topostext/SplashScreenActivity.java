package com.pavla.topostext;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pavla.ToposTextDatabase.ExternalDbOpenHelper;

//import static android.icu.text.RelativeDateTimeFormatter.Direction.THIS;

public class SplashScreenActivity extends ActionBarActivity {

	// Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private static final String DB_NAME = "ToposTextDatabaseAndroid.sqlite";
    private SQLiteDatabase database;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		setContentView(R.layout.activity_splash_screen);


		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
		if (!tabletSize) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}
		
		AsyncTask.execute(new Runnable() {
			   @Override
			   public void run() {
				   ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(SplashScreenActivity.this, DB_NAME);
			       try {
					   database = dbOpenHelper.openDataBase();
					   database.close();

					   runOnUiThread(new Runnable() {
						   @Override
						   public void run() {
							   // This method will be executed once the timer is over
							   // Start apps main activity
							   //Intent i = new Intent(SplashScreenActivity.this, MapActivity.class);
							   //startActivity(i);

							   // close this activity
							   //finish();

							   new Handler().postDelayed(new Runnable() {

                                /*
                                 * Showing splash screen with a timer. This will be useful when you
                                 * want to show case your app logo / company
                                 */

								   @Override
								   public void run() {
									   // This method will be executed once the timer is over
									   // Start apps main activity
									   Intent i = new Intent(SplashScreenActivity.this, MapActivity.class);
									   startActivity(i);

									   // close this activity
									   finish();
								   }
							   }, SPLASH_TIME_OUT);


						   }
					   });



				   }catch (Exception e) {
					   Log.i("LOG","logging catch of an exception for ");
				   }
			        

			   }
			});
		
		
		
		/*new Handler().postDelayed(new Runnable() {
			 
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
 
            /*@Override
            public void run() {
                // This method will be executed once the timer is over
                // Start apps main activity
                Intent i = new Intent(SplashScreenActivity.this, MapActivity.class);
                startActivity(i);
 
                // close this activity
               finish();
            }
        }, SPLASH_TIME_OUT);*/
		
	}

}
