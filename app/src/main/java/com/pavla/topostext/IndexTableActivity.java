package com.pavla.topostext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.text.Text;
import com.pavla.ToposTextDatabase.ExternalDbOpenHelper;
import com.pavla.ToposTextDatabase.IndexTable;
import com.pavla.ToposTextDatabase.Place;
import com.pavla.topostextHelpers.GetRequest;
import com.pavla.topostextHelpers.IndexListAdapter;
import com.pavla.topostextHelpers.IndexReadyListener;
import com.pavla.topostextHelpers.IndexTableLoading;
import com.pavla.topostextHelpers.WorksListAdapter;


import static android.R.attr.id;
import static android.os.AsyncTask.execute;
import static com.pavla.topostext.R.drawable.hits;
import static com.pavla.topostext.R.id.categoryFiltersDone;
import static com.pavla.topostext.R.id.email;
import static com.pavla.topostext.R.id.feedback_comment;
import static com.pavla.topostext.R.id.modernDescription;
import static com.pavla.topostext.R.id.modernDescriptionButton;
import static com.pavla.topostext.R.id.placeName;
import static com.pavla.topostext.R.id.secondScrollView;
import static com.pavla.topostext.R.id.typeFiltersDone;
import static com.pavla.topostext.R.id.worksListview;
//import com.pavla.topostextHelpers.ExpandableIndexListAdapter;


@SuppressLint("NewApi")
public class IndexTableActivity extends ActionBarActivity implements OnClickListener,IndexReadyListener, OnItemClickListener, OnChildClickListener, AdapterView.OnItemSelectedListener {

	private SQLiteDatabase database;
	
	private static final String DB_NAME = "ToposTextDatabaseAndroid.sqlite";
    
	private static final String TABLE_NAME = "ZPLACES";
    private static final String TABLE_MODERN_DESCRIPTION = "ZMODERNDESCCREDITS";
    private static final String CREDITS_STATEMENT = "ZCREDITSTATEMENT";
    private static final String ZDISPLAY_NAME = "ZDISPLAYNAME";
    private static final String DESCRIPTION_ID = "_id";
	private static final String CONFIDENCE = "ZCONFIDENCE";
	private static final String HITS = "ZHITS";
    private static final String PLACE_ID = "_id";
    private static final String LARGE_PLACE_ID = "ZPLACEID";
    private static final String LATITUDE = "ZLATITUDE";
    private static final String LONGITUDE = "ZLONGITUDE";
    private static final String PLACE_NAME = "ZDISPLAYNAME";
    private static final String PLACE_DESCRIPTION = "ZDESCR";
    private static final String PLACE_MODERN_DESCRIPTION = "ZMODERDESCRIPTION";
    private static final String PLACE_MODERN_DESCRIPTION_CREDITS= "ZMODERNDESCRIPTIONCREDITS";
    private static final String TRAVELOGUES_ID= "ZTRAVELOGUESID";
    
    private static String traveloguesID;
    public static String placeName;

    
    private static Place currentPlace;
    private static ListView indexList;
    private static ExpandableListView expIndexList;
    private static ProgressBar progressbar;
    private static TextView hitsNum;
	private static LinearLayout filtersLayout;
	private static TextView filters_row, modernDescription;
    private static ArrayList<IndexTable> myFirstIndexes;
    private static ArrayList<String> myAuthorsList = new ArrayList<String>();
    private static String selectedAuthorNameTitle ;
    private static Button modernDescriptionButton;

    private PopupWindow popupWindowModern,popupWindow;
    //private static ExpandableIndexListAdapter listAdapter;
    private static IndexListAdapter listAdapter;
    private static RadioGroup radiofirstgroup,radiosecondgroup;
    private static ImageButton traveloguesButton;
    private static RadioButton defaultOrder,eventOrder,authorOrder,ascending,descending;
    private static CheckBox adjective,person,placename,ethnic,historians,myth,geographers,inscriptions,nature,philosophy,reference,greekl,latinl;
    private static Boolean historyChecked = false,mythChecked = false,geographersChecked = false,inscriptionsChecked = false,natureChecked = false,philosophyChecked = false,referenceChecked = false;
	private static Boolean adjectiveChecked = false, personChecked = false, placenameChecked = false, ethnicChecked = false;
    private static Boolean latinChecked = false, greekChecked = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_index_table);


        Log.i("placeName","is: "+placeName);

        getSupportActionBar().setIcon(R.drawable.logo_laskaridis_s);
        getSupportActionBar().setLogo(R.drawable.logo_laskaridis_s);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().show();
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

		//get the layout elements
		hitsNum = (TextView) findViewById(R.id.numberOfHits);
		TextView placeTitle = (TextView) findViewById(R.id.workdTitleConfidence);
		TextView shortDescr = (TextView) findViewById(R.id.shortDescription);
		modernDescription = (TextView) findViewById(R.id.modernDescription);
		TextView sortbytitle = (TextView) findViewById(R.id.sortbytitle);
		TextView filterbytitle = (TextView) findViewById(R.id.filtersTitle);
		indexList = (ListView) findViewById(R.id.indexListview);
		expIndexList = (ExpandableListView) findViewById(R.id.expandableindexList);
		Button feedback = (Button) findViewById(R.id.feedbackbtn);
		feedback.setOnClickListener(this);

		traveloguesButton = (ImageButton) findViewById(R.id.traveloguesButton);
		filtersLayout = (LinearLayout) findViewById(R.id.filtersLayout);
		filters_row = (TextView) findViewById(R.id.filters_row);
        modernDescriptionButton = (Button) findViewById(R.id.modernDescriptionButton);

        //Filter buttons
        Button authorFilter = (Button) findViewById(R.id.authorFilter);
        Button categoryFilter = (Button) findViewById(R.id.categoryFilter);
        Button typeFilter = (Button) findViewById(R.id.typeFilter);
        Button languageFilter = (Button) findViewById(R.id.languageFilter);
        Button sortFilter = (Button) findViewById(R.id.sortFilter);
        authorFilter.setOnClickListener(this);
        categoryFilter.setOnClickListener(this);
        typeFilter.setOnClickListener(this);
        languageFilter.setOnClickListener(this);
        sortFilter.setOnClickListener(this);


		//modernDescription.setOnClickListener(this);
        modernDescriptionButton.setOnClickListener(this);
		traveloguesButton.setOnClickListener(this);


		//checkFilters(false);



		progressbar = (ProgressBar) findViewById(R.id.progressBar);
		progressbar.setVisibility(View.GONE);


		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;
		if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
			    expIndexList.setIndicatorBounds(expIndexList.getWidth()-10, expIndexList.getWidth()-5);
			} else {
				expIndexList.setIndicatorBoundsRelative(expIndexList.getWidth()-10, expIndexList.getWidth()-5);
			}

		expIndexList.setOnChildClickListener(this);

		//get the database object
		 ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(this, DB_NAME);
	     database = dbOpenHelper.openDataBase();


		//get Extras
		//Intent i = getIntent();
		//String placeName = i.getExtras().getString("placename");

        Log.e("placeName","is: "+placeName);
		currentPlace = readPlaceWithName(placeName);
		progressbar.setVisibility(View.VISIBLE);
		new IndexTableLoading(this,database,this,progressbar).execute(currentPlace);

		placeTitle.setText(currentPlace.getName() +"	Location Confidence: "+currentPlace.getConfidence());
		shortDescr.setText(Html.fromHtml(currentPlace.getDescription()));
		shortDescr.setMovementMethod(LinkMovementMethod.getInstance());
		/*if	(currentPlace.getModerndescr().length() >0 ){
			modernDescription.setText(noTrailingwhiteLines(Html.fromHtml("<b>Modern Description:</b>"+currentPlace.getModerndescr())));
		}else{
			modernDescription.setVisibility(View.GONE);
		}*/
		hitsNum.setText(currentPlace.getHits()+"\nHits");

		if (currentPlace.getTraveloguesid().length()<=0){
			traveloguesButton.setVisibility(View.GONE);
		}else{
			traveloguesID = currentPlace.getTraveloguesid();
		}

		if (currentPlace.getModerndescr().length() == 0){
            modernDescriptionButton.setVisibility(View.GONE);
        }

		selectedAuthorNameTitle = "";

        Typeface mycustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/NotoSerif-Regular.ttf");
        modernDescription.setTypeface(mycustomFont);

	}



	public void onDestroy(){
        super.onDestroy();
        myAuthorsList.clear();
    }

    public void onStop(){
        super.onStop();
        adjectiveChecked = false;
        ethnicChecked = false;
        geographersChecked = false;
        greekChecked = false;
        historyChecked = false;
        inscriptionsChecked = false;
        latinChecked = false;
        mythChecked = false;
        natureChecked = false;
        personChecked = false;
        philosophyChecked = false;
        placenameChecked = false;
        referenceChecked = false;
    }
	
	/*private void checkFilters(boolean flag){
		defaultOrder.setClickable(flag);
		eventOrder.setClickable(flag);
		authorOrder.setClickable(flag);
		ascending.setClickable(flag);
		descending.setClickable(flag);
	}*/
	
	private CharSequence noTrailingwhiteLines(CharSequence text) {

	    while (text.charAt(text.length() - 1) == '\n') {
	        text = text.subSequence(0, text.length() - 1);
	    }
	    return text;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.index_table, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.showFilters) {
			if (filtersLayout.getVisibility() == View.VISIBLE){
				filtersLayout.setVisibility(View.GONE);
				filters_row.setVisibility(View.GONE);
			}else {
				filtersLayout.setVisibility(View.VISIBLE);
				filters_row.setVisibility(View.VISIBLE);
			}
			return true;
		}else if (id == R.id.driveme){
            new AlertDialog.Builder(this)
                    .setMessage(getResources().getString(R.string.sure_drive_me))
                    .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            Place currentPlaceLocation = null;
                            try{
                                currentPlaceLocation = currentPlace;//getPlaceCoordinates(currentPlaceID);
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" +String.valueOf(currentPlaceLocation.getLatitude())+","+String.valueOf(currentPlaceLocation.getLongitude())));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }else if (id == R.id.feedback){
            showFeedbackpopup();
        }
		return super.onOptionsItemSelected(item);
	}

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
            case R.id.sendFeedback:
                View pv = popupWindow.getContentView();
                EditText emailet = (EditText) pv.findViewById(R.id.feedback_email);
                EditText commentset = (EditText) pv.findViewById(R.id.feedback_comment);
                RatingBar ratingFeedback = (RatingBar) pv.findViewById(R.id.feedbackRating);
                if (currentPlace.getPlaceid().length() != 0 && commentset.getText().length() != 0 && emailet.getText().length() != 0 ){
                    if (isValidEmail(emailet.getText())) {
                        new GetRequest(this).execute("http://topostext.org/rest/feedback.php?placeID=" + currentPlace.getPlaceid() + "&rating=" + ratingFeedback.getRating() + "&comments=" + commentset.getText() + "&email=" + emailet.getText());
                        popupWindow.dismiss();
                    }else{
                        Toast.makeText(this,getResources().getString(R.string.not_valid_email),Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(this,getResources().getString(R.string.fill_all_fields),Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.sortDefault:
                switch	(radiosecondgroup.getCheckedRadioButtonId()){
                    case R.id.ascending:
                        Collections.sort(myFirstIndexes, IndexTable.DefaultComparatorASC);
                        break;
                    case R.id.descending:
                        Collections.sort(myFirstIndexes, IndexTable.DefaultComparatorDESC);
                        break;
                }

                listAdapter.update(myFirstIndexes);
                listAdapter.notifyDataSetChanged();
                prepareToFilterData();
                break;
            case R.id.sortdate:
                switch	(radiosecondgroup.getCheckedRadioButtonId()){
                    case R.id.ascending:
                        Collections.sort(myFirstIndexes, IndexTable.EventDateComparatorASC);
                        break;
                    case R.id.descending:
                        Collections.sort(myFirstIndexes, IndexTable.EventDateComparatorDESC);
                        break;
                }

                listAdapter.update(myFirstIndexes);
                listAdapter.notifyDataSetChanged();
                prepareToFilterData();
                break;
            case R.id.sortAuthor:
                switch	(radiosecondgroup.getCheckedRadioButtonId()){
                    case R.id.ascending:
                        Collections.sort(myFirstIndexes, IndexTable.AuthorComparatorASC);
                        break;
                    case R.id.descending:
                        Collections.sort(myFirstIndexes, IndexTable.AuthorComparatorDESC);
                        break;
                }

                listAdapter.update(myFirstIndexes);
                listAdapter.notifyDataSetChanged();
                prepareToFilterData();
                break;
            case R.id.ascending:
                switch	(radiofirstgroup.getCheckedRadioButtonId()){
                case R.id.sortDefault:
                    Collections.sort(myFirstIndexes, IndexTable.DefaultComparatorASC);
                    break;
                case R.id.sortdate:
                    Collections.sort(myFirstIndexes, IndexTable.EventDateComparatorASC);
                    break;
                case R.id.sortAuthor:
                    Collections.sort(myFirstIndexes, IndexTable.AuthorComparatorASC);
                    break;
                }
                listAdapter.update(myFirstIndexes);
                listAdapter.notifyDataSetChanged();
                prepareToFilterData();
                break;
            case R.id.descending:
                switch	(radiofirstgroup.getCheckedRadioButtonId()){
                case R.id.sortDefault:
                    Collections.sort(myFirstIndexes, IndexTable.DefaultComparatorDESC);
                    break;
                case R.id.sortdate:
                    Collections.sort(myFirstIndexes, IndexTable.EventDateComparatorDESC);
                    break;
                case R.id.sortAuthor:
                    Collections.sort(myFirstIndexes, IndexTable.AuthorComparatorDESC);
                    break;
                }
                listAdapter.update(myFirstIndexes);
                listAdapter.notifyDataSetChanged();
                prepareToFilterData();
                break;
            case R.id.filterperson:
            case R.id.filterethnic:
            case R.id.filteradj:
            case R.id.filterplacename:
            case R.id.filterhistorians:
            case R.id.filtermyth:
            case R.id.filtergeographers:
                prepareToFilterData();
                break;
            /*case modernDescription:
                Intent j = new Intent(IndexTableActivity.this,ModernDescriptionActivity.class);
                j.putExtra("currentplaceName",currentPlace.getName());
                j.putExtra("currentPlaceID", currentPlace.getPlaceid());
                j.putExtra("creditsID", currentPlace.getModerndescrcreditsid());
                j.putExtra("currentplaceModernDescription", currentPlace.getModerndescr());
                startActivity(j);
                break;*/
            case R.id.traveloguesButton:
                try{
                String url = "http://eng.travelogues.gr/tag.php?view="+traveloguesID;
                Intent urlintent = new Intent(Intent.ACTION_VIEW);
                urlintent.setData(Uri.parse(url));
                startActivity(urlintent);
                }catch(Exception ex){
                    Toast.makeText(getApplicationContext(), ex.toString() + ""+ ex.getLocalizedMessage() +""+ex.getMessage() +"", Toast.LENGTH_LONG).show();
                }
			    break;
            case R.id.modernDescriptionButton:
                showModernDescription();
                break;
            case R.id.clickedBg:
                popupWindowModern.dismiss();
                break;
            case R.id.clickableBackground:
            case R.id.categoryFiltersDone:
            case R.id.typeFiltersDone:
            case R.id.sortFiltersDone:
            case R.id.languageFiltersDone:
                popupWindow.dismiss();
                prepareToFilterData();
                break;
            case R.id.categoryFilter:
                showCategoryFilters();
                break;
            case R.id.typeFilter:
                showTypeFilters();
                break;
            case R.id.sortFilter:
                showSortByPopup();
                break;
            case R.id.authorFilter:
                showAuthorsPopup();
                break;
            case R.id.languageFilter:
                showLanguageFilters();
		}
	}

	private void showModernDescription(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.modern_description, null);
        //final PopupWindow
        popupWindowModern = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickedBg);
        TextView modernDescr = (TextView) popupView.findViewById(R.id.modernDescription);
        //modernDescr.setText(currentPlace.getModerndescr());
        Typeface mycustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/NotoSerif-Regular.ttf");
        modernDescr.setTypeface(mycustomFont);
        Log.i("LOG","MODERN DESCRIPTION : "+currentPlace.getModerndescr());
        modernDescr.setText(Html.fromHtml(currentPlace.getModerndescr() +"<br/><i>"+ readCreditWithID(String.valueOf(currentPlace.getModerndescrcreditsid()))+"</i>"));
        modernDescr.setMovementMethod(LinkMovementMethod.getInstance());
        clickedBG.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindowModern.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }

    private void showCategoryFilters(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.category_filters_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);
        historians = (CheckBox) popupView.findViewById(R.id.filterhistorians);
        myth = (CheckBox) popupView.findViewById(R.id.filtermyth);
        geographers = (CheckBox) popupView.findViewById(R.id.filtergeographers);
        inscriptions = (CheckBox) popupView.findViewById(R.id.filterinscriptions);
        nature = (CheckBox) popupView.findViewById(R.id.filternature);
        philosophy = (CheckBox) popupView.findViewById(R.id.filterphilosophy);
        reference = (CheckBox) popupView.findViewById(R.id.filterreference);
        Button categoryFiltersDone = (Button) popupView.findViewById(R.id.categoryFiltersDone);
        clickedBG.setOnClickListener(this);

        inscriptions.setOnClickListener(this);
        nature.setOnClickListener(this);
        philosophy.setOnClickListener(this);
        reference.setOnClickListener(this);

        historians.setChecked(historyChecked);
        myth.setChecked(mythChecked);
        geographers.setChecked(geographersChecked);
        inscriptions.setChecked(inscriptionsChecked);
        nature.setChecked(natureChecked);
        philosophy.setChecked(philosophyChecked);
        reference.setChecked(referenceChecked);

        /*adjective.setClickable(flag);
        person.setClickable(flag);
        placename.setClickable(flag);
        ethnic.setClickable(flag);
        historians.setClickable(flag);
        myth.setClickable(flag);
        geographers.setClickable(flag);*/

        categoryFiltersDone.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }

    private void showTypeFilters(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.type_filters_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);
        adjective = (CheckBox) popupView.findViewById(R.id.filteradjective);
        person = (CheckBox) popupView.findViewById(R.id.filterperson);
        placename = (CheckBox) popupView.findViewById(R.id.filterplacename);
        ethnic = (CheckBox) popupView.findViewById(R.id.filterethnic);
        Button typeFiltersDone = (Button) popupView.findViewById(R.id.typeFiltersDone);
        clickedBG.setOnClickListener(this);

        adjective.setChecked(adjectiveChecked);
        person.setChecked(personChecked);
        placename.setChecked(placenameChecked);
        ethnic.setChecked(ethnicChecked);

        typeFiltersDone.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }


    private void showLanguageFilters(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.language_filters_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);
        greekl = (CheckBox) popupView.findViewById(R.id.greek_filter);
        latinl = (CheckBox) popupView.findViewById(R.id.latin_filter);
        Button languageFiltersDone = (Button) popupView.findViewById(R.id.languageFiltersDone);
        clickedBG.setOnClickListener(this);

        greekl.setChecked(greekChecked);
        latinl.setChecked(latinChecked);

        languageFiltersDone.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }

    private void showSortByPopup(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.sort_filters_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);

        int selectedFirstgroup = 0;
        int selectedSecondGroup = 0;
        if (radiofirstgroup != null){
            if (defaultOrder.isChecked()) selectedFirstgroup = 0;
            if (eventOrder.isChecked()) selectedFirstgroup = 1;
            if (authorOrder.isChecked()) selectedFirstgroup = 2;
        }

        if (radiosecondgroup != null){
            if (ascending.isChecked()) selectedSecondGroup = 0;
            if (descending.isChecked()) selectedSecondGroup = 1;
        }

        radiofirstgroup = (RadioGroup) popupView.findViewById(R.id.sorting);
        radiosecondgroup = (RadioGroup) popupView.findViewById(R.id.ascDesc);
        defaultOrder = (RadioButton) popupView.findViewById(R.id.sortDefault);
        eventOrder = (RadioButton) popupView.findViewById(R.id.sortdate);
        authorOrder = (RadioButton) popupView.findViewById(R.id.sortAuthor);
        ascending = (RadioButton) popupView.findViewById(R.id.ascending);
        descending = (RadioButton) popupView.findViewById(R.id.descending);

        defaultOrder.setOnClickListener(this);
        eventOrder.setOnClickListener(this);
        authorOrder.setOnClickListener(this);
        ascending.setOnClickListener(this);
        descending.setOnClickListener(this);

        switch (selectedFirstgroup){
            case 0:
                defaultOrder.setChecked(true);
                break;
            case 1:
                eventOrder.setChecked(true);
                break;
            case 2:
                authorOrder.setChecked(true);
                break;
            default:
                break;
        }

        switch (selectedSecondGroup){
            case 0:
                ascending.setChecked(true);
                break;
            case 1:
                descending.setChecked(true);
                break;
            default:
                break;
        }

        Button sortFiltersDone = (Button) popupView.findViewById(R.id.sortFiltersDone);
        clickedBG.setOnClickListener(this);



        sortFiltersDone.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }

    private void showAuthorsPopup(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.authors_filters_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);

        ListView worksListView = (ListView) popupView.findViewById(R.id.authorsListView);
        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, R.layout.authors_list_row,myAuthorsList);
        worksListView.setAdapter(listAdapter);
        worksListView.setOnItemClickListener(this);
        worksListView.setClickable(true);

        clickedBG.setOnClickListener(this);


        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }

    private void showFeedbackpopup(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.feedback_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        popupWindow.setFocusable(true);
        popupWindow.update();

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);

        TextView currentPlaceName = (TextView) popupView.findViewById(R.id.dynamic_name);
        currentPlaceName.setText(currentPlace.getName());

        Button sendFeedbackButton = (Button) popupView.findViewById(R.id.sendFeedback);
        sendFeedbackButton.setOnClickListener(this);

        clickedBG.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }
	
	
	private void prepareToFilterData(){
		ArrayList<String> typesArray = new ArrayList<String>();
		ArrayList<String> categoryArray = new ArrayList<String>();
        ArrayList<String> languagesArray = new ArrayList<String>();
		if	(adjective != null && adjective.isChecked()){
            adjectiveChecked = true;
			typesArray.add("demonym");
		}else adjectiveChecked = false;

		if (person != null && person.isChecked()){
            personChecked = true;
			typesArray.add("person");
		}else personChecked = false;

		if (placename != null && placename.isChecked()){
            placenameChecked = true;
			typesArray.add("place");
		}else placenameChecked = false;

		if	(ethnic != null && ethnic.isChecked()){
            ethnicChecked = true;
			typesArray.add("ethnic");
		}else ethnicChecked = false;



        if	(greekl != null && greekl.isChecked()){
            greekChecked = true;
            languagesArray.add("Greek");
        }else greekChecked = false;

        if (latinl != null && latinl.isChecked()){
            latinChecked = true;
            languagesArray.add("Latin");
        }else latinChecked = false;

		
		
		if (historians != null && historians.isChecked()){
            historyChecked = true;
			categoryArray.add("history");
		}else historyChecked = false;

		if (myth != null && myth.isChecked()){
            mythChecked = true;
			categoryArray.add("myth-literature");
		}else mythChecked = false;

		if	(geographers != null && geographers.isChecked()){
            geographersChecked = true;
			categoryArray.add("geography");
		}else geographersChecked = false;

        if	(inscriptions != null && inscriptions.isChecked()){
            inscriptionsChecked = true;
            categoryArray.add("inscription");
        }else inscriptionsChecked = false;

        if	(nature != null && nature.isChecked()){
            natureChecked = true;
            categoryArray.add("nature");
        }else natureChecked = false;

        if	(philosophy != null && philosophy.isChecked()){
            philosophyChecked = true;
            categoryArray.add("philosophy");
        }else philosophyChecked = false;

        if	(reference != null && reference.isChecked()){
            referenceChecked = true;
            categoryArray.add("reference");
        }else referenceChecked = false;

        final ArrayList<String> tArray = typesArray;
        final ArrayList<String> catArray = categoryArray;
        final ArrayList<String> lArray = languagesArray;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listAdapter.filterData(tArray, catArray, lArray, selectedAuthorNameTitle);
                listAdapter.notifyDataSetChanged();
            }
        });
		hitsNum.setText(listAdapter.totalHits+"\nHits");
	}
	
	private Place readPlaceWithName(String placeName) {
		// TODO Auto-generated method stub
		
		//routes = new ArrayList<String>();
	    Cursor routesCursor = database.query(TABLE_NAME, new String[] {PLACE_ID,
	            PLACE_NAME,PLACE_DESCRIPTION,CONFIDENCE,HITS,LATITUDE,LONGITUDE,LARGE_PLACE_ID,PLACE_MODERN_DESCRIPTION,PLACE_MODERN_DESCRIPTION_CREDITS,TRAVELOGUES_ID}, PLACE_NAME + " = '"+placeName+"'", null, null, null, PLACE_NAME);
	        routesCursor.moveToFirst();
	        //if(!routesCursor.isAfterLast()) {

	            	int id = routesCursor.getInt(0);
	                String name = routesCursor.getString(1);
	                String description = routesCursor.getString(2);
	                String confidence = routesCursor.getString(3);
	                int hits = routesCursor.getInt(4);
	                float latitude = routesCursor.getFloat(5);
	                float longitude = routesCursor.getFloat(6);
	                String placeID = routesCursor.getString(7);
	                String placemodernDesc = routesCursor.getString(8);
	                int placemoderndesccredits = routesCursor.getInt(9);
	                String traveloguesID = routesCursor.getString(10);
	                //String image_name = routesCursor.getString(5);
	                routesCursor.close();
	                return new Place(id,name,description,confidence,hits,latitude,longitude,placeID,placemodernDesc,placemoderndesccredits,traveloguesID);

	        //}

	}

    private String readCreditWithID(String creditID) {
        // TODO Auto-generated method stub

        //routes = new ArrayList<String>();
        Cursor routesCursor = database.query(TABLE_MODERN_DESCRIPTION, new String[] {ZDISPLAY_NAME,
                CREDITS_STATEMENT}, DESCRIPTION_ID + " = '"+creditID+"'", null, null, null, null);
        routesCursor.moveToFirst();
        //if(!routesCursor.isAfterLast()) {

        String name = routesCursor.getString(0);
        String statement = routesCursor.getString(1);

        routesCursor.close();
        String result = "<b>Credits: </b>"+name+":"+statement;
        return result;

        //}

    }
	
	//Convert pixel to dip 
	public int GetDipsFromPixel(float pixels)
	{
	        // Get the screen's density scale
	        final float scale = getResources().getDisplayMetrics().density;
	        // Convert the dps to pixels, based on density scale
	        return (int) (pixels * scale + 0.5f);
	} 

	@Override
	public void indexReady(ArrayList<IndexTable> theList) {
		progressbar.setVisibility(View.GONE);
		if	 (theList.size() > 0){
            modernDescription.setVisibility(View.GONE);
			listAdapter = new IndexListAdapter(this, R.layout.index_row,theList);
			indexList.setAdapter(listAdapter);
			listAdapter.notifyDataSetChanged();
			//checkFilters(true);
			indexList.setOnItemClickListener(this);
			myFirstIndexes = theList;

            myAuthorsList.add("ALL");
            for (IndexTable obj : theList) {
                if (!myAuthorsList.contains(obj.getWorkTitle())){
                    myAuthorsList.add(obj.getWorkTitle());
                }

            }
		}else{
            if (currentPlace.getModerndescr() != "" && currentPlace.getModerndescr() != null){
                modernDescription.setVisibility(View.VISIBLE);
                Typeface mycustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/NotoSerif-Regular.ttf");
                modernDescription.setTypeface(mycustomFont);
                modernDescription.setText(Html.fromHtml(currentPlace.getModerndescr()+"<br/>"+ readCreditWithID(String.valueOf(currentPlace.getModerndescrcreditsid()))));
                modernDescription.setMovementMethod(LinkMovementMethod.getInstance());
                modernDescriptionButton.setVisibility(View.GONE);
            }
            else{
                modernDescription.setVisibility(View.GONE);
            }
        }
		
	}
	
	public void expandableIndexReady(ArrayList<IndexTable> theList, HashMap<String,ArrayList<IndexTable>> hashmap) {

		
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
        if (indexList.getAdapter() != parent.getAdapter()){
            selectedAuthorNameTitle = myAuthorsList.get(position);
            prepareToFilterData();
            popupWindow.dismiss();
        }else{
            IndexTable ind = (IndexTable) indexList.getAdapter().getItem(position);//myFirstIndexes.get(position);

            //TextView paragraphNumber = (TextView) view.findViewById(R.id.paragraphnumber);
            Intent intent = new Intent(IndexTableActivity.this,FullWorkActivity.class);
            intent.putExtra("myWorkName",ind.getWorkTitle().replace("'","''"));
            intent.putExtra("paragraphID", Integer.toString(ind.getParagraph()));
            intent.putExtra("placeID", currentPlace.getPlaceid());
            intent.putExtra("placeName", ind.getName());
            IndexListAdapter indexAdapter = (IndexListAdapter)indexList.getAdapter();
            intent.putExtra("hitsList", indexAdapter.getListItems());//myFirstIndexes);
            //intent.putExtra("hashmapList", myHashmap);
            intent.putExtra("currentPosition", position);
            //intent.putExtra("childPosition",1);
            intent.putExtra("totalHits", indexList.getAdapter().getCount());//myFirstIndexes.size());
            startActivity(intent);
        }

	}


	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "index clicked group position" +groupPosition +" and child position: "+childPosition, Toast.LENGTH_LONG).show();
		return false;
	}

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}


