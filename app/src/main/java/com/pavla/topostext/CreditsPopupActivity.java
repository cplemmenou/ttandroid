package com.pavla.topostext;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;

public class CreditsPopupActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_credits_popup);
		
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		
		int width = dm.widthPixels;
		int height = dm.heightPixels;
		
		getWindow().setLayout((int)(width*.8),(int)(height*.6));
		
		TextView creditsTextView = (TextView) findViewById(R.id.creditsText);
		Intent i = getIntent();
		if	(i.hasExtra("creditsStatement")){
			Typeface mycustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/NotoSerif-Regular.ttf");
			creditsTextView.setTypeface(mycustomFont);
			String text = i.getExtras().getString("creditsStatement");
			creditsTextView.setText(noTrailingwhiteLines(Html.fromHtml(text)));
			creditsTextView.setMovementMethod(LinkMovementMethod.getInstance());
			//creditsTextView.setAutoLinkMask(Linkify.WEB_URLS);
		}
	}
	
	private CharSequence noTrailingwhiteLines(CharSequence text) {

	    while (text.charAt(text.length() - 1) == '\n') {
	        text = text.subSequence(0, text.length() - 1);
	    }
	    return text;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.credits_popup, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
