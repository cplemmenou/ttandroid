package com.pavla.topostext;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pavla.ToposTextDatabase.ExternalDbOpenHelper;
import com.pavla.ToposTextDatabase.IndexTable;
import com.pavla.ToposTextDatabase.Person;
import com.pavla.topostextHelpers.IndexListAdapter;
import com.pavla.topostextHelpers.IndexReadyListener;
import com.pavla.topostextHelpers.IndexTableLoading;
import com.pavla.topostextHelpers.PersonIndexTableLoading;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static android.R.attr.description;
import static android.R.attr.name;
import static com.pavla.topostext.R.id.modernDescription;
import static com.pavla.topostext.R.id.modernDescriptionButton;
import static com.pavla.topostext.R.id.placeName;
import static com.pavla.topostext.R.id.traveloguesButton;
//import com.pavla.topostextHelpers.ExpandableIndexListAdapter;

@SuppressLint("NewApi")
public class PeopleIndexTableActivity extends ActionBarActivity implements OnClickListener,IndexReadyListener, OnItemClickListener, OnChildClickListener, AdapterView.OnItemSelectedListener {

	private SQLiteDatabase database;
    public static String personName;
	
	private static final String DB_NAME = "ToposTextDatabaseAndroid.sqlite";
    
	private static final String TABLE_NAME = "ZPERSON";
	private static final String CENTURY = "ZCENTURY";
	private static final String GENDER = "ZGENDER";
    private static final String PERSON_ID = "_id";
    private static final String PERSONID_CUSTOM = "ZPERSONID";
    private static final String HITS = "ZHITS";
    private static final String PERSON_NAME = "ZNAME";
    private static final String PERIOD = "ZPERIOD";
    private static final String WIKILINK = "ZWIKI";
    
    private static String wikiID;

    
    private static Person currentPerson;
    private static ListView indexList;
    private static ExpandableListView expIndexList;
    private static ProgressBar progressbar;
    private static TextView hitsNum;
	private static LinearLayout filtersLayout;
	private static TextView filters_row;
    private static ArrayList<IndexTable> myFirstIndexes;
    private static ArrayList<String> myAuthorsList = new ArrayList<String>();
    private static String selectedAuthorNameTitle ;

    private PopupWindow popupWindowModern,popupWindow;
    //private static ExpandableIndexListAdapter listAdapter;
    private static IndexListAdapter listAdapter;
    private static RadioGroup radiofirstgroup,radiosecondgroup;
    private static ImageButton wikiButton;
    private static RadioButton defaultOrder,eventOrder,authorOrder,ascending,descending;
    private static CheckBox patronym,person,placename,ethnic,historians,myth,geographers,inscriptions,nature,philosophy,reference,greekl,latinl;
    private static Boolean historyChecked = false,mythChecked = false,geographersChecked = false,inscriptionsChecked = false,natureChecked = false,philosophyChecked = false,referenceChecked = false;
	private static Boolean patronymChecked = false, personChecked = false, placenameChecked = false, ethnicChecked = false;
    private static Boolean latinChecked = false, greekChecked = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_people_index_table);

        getSupportActionBar().setIcon(R.drawable.logo_laskaridis_s);
        getSupportActionBar().setLogo(R.drawable.logo_laskaridis_s);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().show();
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
		
		//get the layout elements
		hitsNum = (TextView) findViewById(R.id.numberOfHits);
		TextView personTitle = (TextView) findViewById(R.id.workdTitleConfidence);

		TextView sortbytitle = (TextView) findViewById(R.id.sortbytitle);
		TextView filterbytitle = (TextView) findViewById(R.id.filtersTitle);
		indexList = (ListView) findViewById(R.id.indexListview);

		wikiButton = (ImageButton) findViewById(R.id.wikiButton);
		filtersLayout = (LinearLayout) findViewById(R.id.filtersLayout);
		filters_row = (TextView) findViewById(R.id.filters_row);

        //Filter buttons
        Button authorFilter = (Button) findViewById(R.id.authorFilter);
        Button categoryFilter = (Button) findViewById(R.id.categoryFilter);
        Button typeFilter = (Button) findViewById(R.id.typeFilter);
        Button languageFilter = (Button) findViewById(R.id.languageFilter);
        Button sortFilter = (Button) findViewById(R.id.sortFilter);
        authorFilter.setOnClickListener(this);
        categoryFilter.setOnClickListener(this);
        typeFilter.setOnClickListener(this);
        languageFilter.setOnClickListener(this);
        sortFilter.setOnClickListener(this);
		


		wikiButton.setOnClickListener(this);

		
		progressbar = (ProgressBar) findViewById(R.id.progressBar);
		progressbar.setVisibility(View.GONE);
		
		//get the database object
		 ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(this, DB_NAME);
	     database = dbOpenHelper.openDataBase();
		
		
		//get Extras
		//Intent i = getIntent();
		//String peopleName = i.getExtras().getString("personname");
		

		currentPerson = readPersonWithName(personName);
		progressbar.setVisibility(View.VISIBLE);
		new PersonIndexTableLoading(this,database,this,progressbar).execute(currentPerson);
		
		personTitle.setText(currentPerson.getName());


		hitsNum.setText(currentPerson.getHits()+"\nHits");
		
		if (currentPerson.getWikiLink().length()<=0){
			wikiButton.setVisibility(View.GONE);
		}else{
			wikiID = currentPerson.getWikiLink();
		}
		
		selectedAuthorNameTitle = "";

	}


	public void onStop(){
        super.onStop();
        if (getIntent().hasExtra("previousActivity")){
            finish();
        }

        ethnicChecked = false;
        geographersChecked = false;
        greekChecked = false;
        historyChecked = false;
        inscriptionsChecked = false;
        latinChecked = false;
        mythChecked = false;
        natureChecked = false;
        personChecked = false;
        philosophyChecked = false;
        placenameChecked = false;
        referenceChecked = false;
        patronymChecked = false;

        myAuthorsList.clear();
    }
	
	/*private void checkFilters(boolean flag){
		defaultOrder.setClickable(flag);
		eventOrder.setClickable(flag);
		authorOrder.setClickable(flag);
		ascending.setClickable(flag);
		descending.setClickable(flag);
	}*/
	
	private CharSequence noTrailingwhiteLines(CharSequence text) {

	    while (text.charAt(text.length() - 1) == '\n') {
	        text = text.subSequence(0, text.length() - 1);
	    }
	    return text;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.people_index_table, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.showFilters) {
			if (filtersLayout.getVisibility() == View.VISIBLE){
				filtersLayout.setVisibility(View.GONE);
				filters_row.setVisibility(View.GONE);
			}else {
				filtersLayout.setVisibility(View.VISIBLE);
				filters_row.setVisibility(View.VISIBLE);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){

            case R.id.sortDefault:
                switch	(radiosecondgroup.getCheckedRadioButtonId()){
                    case R.id.ascending:
                        Collections.sort(myFirstIndexes, IndexTable.DefaultComparatorASC);
                        break;
                    case R.id.descending:
                        Collections.sort(myFirstIndexes, IndexTable.DefaultComparatorDESC);
                        break;
                }

                listAdapter.update(myFirstIndexes);
                listAdapter.notifyDataSetChanged();
                prepareToFilterData();
                break;
            case R.id.sortdate:
                switch	(radiosecondgroup.getCheckedRadioButtonId()){
                    case R.id.ascending:
                        Collections.sort(myFirstIndexes, IndexTable.EventDateComparatorASC);
                        break;
                    case R.id.descending:
                        Collections.sort(myFirstIndexes, IndexTable.EventDateComparatorDESC);
                        break;
                }

                listAdapter.update(myFirstIndexes);
                listAdapter.notifyDataSetChanged();
                prepareToFilterData();
                break;
            case R.id.sortAuthor:
                switch	(radiosecondgroup.getCheckedRadioButtonId()){
                    case R.id.ascending:
                        Collections.sort(myFirstIndexes, IndexTable.AuthorComparatorASC);
                        break;
                    case R.id.descending:
                        Collections.sort(myFirstIndexes, IndexTable.AuthorComparatorDESC);
                        break;
                }

                listAdapter.update(myFirstIndexes);
                listAdapter.notifyDataSetChanged();
                prepareToFilterData();
                break;
            case R.id.ascending:
                switch	(radiofirstgroup.getCheckedRadioButtonId()){
                case R.id.sortDefault:
                    Collections.sort(myFirstIndexes, IndexTable.DefaultComparatorASC);
                    break;
                case R.id.sortdate:
                    Collections.sort(myFirstIndexes, IndexTable.EventDateComparatorASC);
                    break;
                case R.id.sortAuthor:
                    Collections.sort(myFirstIndexes, IndexTable.AuthorComparatorASC);
                    break;
                }
                listAdapter.update(myFirstIndexes);
                listAdapter.notifyDataSetChanged();
                prepareToFilterData();
                break;
            case R.id.descending:
                switch	(radiofirstgroup.getCheckedRadioButtonId()){
                case R.id.sortDefault:
                    Collections.sort(myFirstIndexes, IndexTable.DefaultComparatorDESC);
                    break;
                case R.id.sortdate:
                    Collections.sort(myFirstIndexes, IndexTable.EventDateComparatorDESC);
                    break;
                case R.id.sortAuthor:
                    Collections.sort(myFirstIndexes, IndexTable.AuthorComparatorDESC);
                    break;
                }
                listAdapter.update(myFirstIndexes);
                listAdapter.notifyDataSetChanged();
                prepareToFilterData();
                break;
            case R.id.filterperson:
            case R.id.filterethnic:
            case R.id.filteradj:
            case R.id.filterplacename:
            case R.id.filterhistorians:
            case R.id.filtermyth:
            case R.id.filtergeographers:
                prepareToFilterData();
                break;
            case R.id.clickedBg:
                popupWindowModern.dismiss();
                break;
            case R.id.clickableBackground:
            case R.id.categoryFiltersDone:
            case R.id.typeFiltersDone:
            case R.id.sortFiltersDone:
            case R.id.languageFiltersDone:
                popupWindow.dismiss();
                prepareToFilterData();
                break;
            case R.id.categoryFilter:
                showCategoryFilters();
                break;
            case R.id.typeFilter:
                showTypeFilters();
                break;
            case R.id.sortFilter:
                showSortByPopup();
                break;
            case R.id.authorFilter:
                showAuthorsPopup();
                break;
            case R.id.languageFilter:
                showLanguageFilters();
                break;
            case R.id.wikiButton:
                try{
                    String url = "https://en.wikipedia.org"+wikiID;
                    Intent urlintent = new Intent(Intent.ACTION_VIEW);
                    urlintent.setData(Uri.parse(url));
                    startActivity(urlintent);
                }catch(Exception ex){
                    Toast.makeText(getApplicationContext(), ex.toString() + ""+ ex.getLocalizedMessage() +""+ex.getMessage() +"", Toast.LENGTH_LONG).show();
                }
                break;
		}
	}


    private void showCategoryFilters(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.category_filters_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);
        historians = (CheckBox) popupView.findViewById(R.id.filterhistorians);
        myth = (CheckBox) popupView.findViewById(R.id.filtermyth);
        geographers = (CheckBox) popupView.findViewById(R.id.filtergeographers);
        inscriptions = (CheckBox) popupView.findViewById(R.id.filterinscriptions);
        nature = (CheckBox) popupView.findViewById(R.id.filternature);
        philosophy = (CheckBox) popupView.findViewById(R.id.filterphilosophy);
        reference = (CheckBox) popupView.findViewById(R.id.filterreference);
        Button categoryFiltersDone = (Button) popupView.findViewById(R.id.categoryFiltersDone);
        clickedBG.setOnClickListener(this);

        inscriptions.setOnClickListener(this);
        nature.setOnClickListener(this);
        philosophy.setOnClickListener(this);
        reference.setOnClickListener(this);

        historians.setChecked(historyChecked);
        myth.setChecked(mythChecked);
        geographers.setChecked(geographersChecked);
        inscriptions.setChecked(inscriptionsChecked);
        nature.setChecked(natureChecked);
        philosophy.setChecked(philosophyChecked);
        reference.setChecked(referenceChecked);

        /*adjective.setClickable(flag);
        person.setClickable(flag);
        placename.setClickable(flag);
        ethnic.setClickable(flag);
        historians.setClickable(flag);
        myth.setClickable(flag);
        geographers.setClickable(flag);*/

        categoryFiltersDone.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }

    private void showTypeFilters(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.type_filters_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);
        patronym = (CheckBox) popupView.findViewById(R.id.filteradjective);
        person = (CheckBox) popupView.findViewById(R.id.filterperson);
        placename = (CheckBox) popupView.findViewById(R.id.filterplacename);
        ethnic = (CheckBox) popupView.findViewById(R.id.filterethnic);
        Button typeFiltersDone = (Button) popupView.findViewById(R.id.typeFiltersDone);
        clickedBG.setOnClickListener(this);

        patronym.setChecked(patronymChecked);
        person.setChecked(personChecked);
        placename.setChecked(placenameChecked);
        ethnic.setChecked(ethnicChecked);

        patronym.setText("patronym");
        placename.setVisibility(View.GONE);
        ethnic.setVisibility(View.GONE);

        typeFiltersDone.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }

    private void showSortByPopup(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.sort_filters_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);

        int selectedFirstgroup = 0;
        int selectedSecondGroup = 0;
        if (radiofirstgroup != null){
            if (defaultOrder.isChecked()) selectedFirstgroup = 0;
            if (eventOrder.isChecked()) selectedFirstgroup = 1;
            if (authorOrder.isChecked()) selectedFirstgroup = 2;
        }

        if (radiosecondgroup != null){
            if (ascending.isChecked()) selectedSecondGroup = 0;
            if (descending.isChecked()) selectedSecondGroup = 1;
        }

        radiofirstgroup = (RadioGroup) popupView.findViewById(R.id.sorting);
        radiosecondgroup = (RadioGroup) popupView.findViewById(R.id.ascDesc);
        defaultOrder = (RadioButton) popupView.findViewById(R.id.sortDefault);
        eventOrder = (RadioButton) popupView.findViewById(R.id.sortdate);
        authorOrder = (RadioButton) popupView.findViewById(R.id.sortAuthor);
        ascending = (RadioButton) popupView.findViewById(R.id.ascending);
        descending = (RadioButton) popupView.findViewById(R.id.descending);

        defaultOrder.setOnClickListener(this);
        eventOrder.setOnClickListener(this);
        authorOrder.setOnClickListener(this);
        ascending.setOnClickListener(this);
        descending.setOnClickListener(this);

        switch (selectedFirstgroup){
            case 0:
                defaultOrder.setChecked(true);
                break;
            case 1:
                eventOrder.setChecked(true);
                break;
            case 2:
                authorOrder.setChecked(true);
                break;
            default:
                break;
        }

        switch (selectedSecondGroup){
            case 0:
                ascending.setChecked(true);
                break;
            case 1:
                descending.setChecked(true);
                break;
            default:
                break;
        }

        Button sortFiltersDone = (Button) popupView.findViewById(R.id.sortFiltersDone);
        clickedBG.setOnClickListener(this);



        sortFiltersDone.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }

    private void showAuthorsPopup(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.authors_filters_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);

        ListView worksListView = (ListView) popupView.findViewById(R.id.authorsListView);
        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, R.layout.authors_list_row,myAuthorsList);
        worksListView.setAdapter(listAdapter);
        worksListView.setOnItemClickListener(this);
        worksListView.setClickable(true);

        clickedBG.setOnClickListener(this);


        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }

    private void showLanguageFilters(){
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.language_filters_popup, null);
        //final PopupWindow
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout clickedBG = (LinearLayout) popupView.findViewById(R.id.clickableBackground);
        greekl = (CheckBox) popupView.findViewById(R.id.greek_filter);
        latinl = (CheckBox) popupView.findViewById(R.id.latin_filter);
        Button languageFiltersDone = (Button) popupView.findViewById(R.id.languageFiltersDone);
        clickedBG.setOnClickListener(this);

        greekl.setChecked(greekChecked);
        latinl.setChecked(latinChecked);

        languageFiltersDone.setOnClickListener(this);

        //popupWindow.showAsDropDown(infoButton, 50, -30);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 30, 30);
    }
	
	
	private void prepareToFilterData(){
		ArrayList<String> typesArray = new ArrayList<String>();
		ArrayList<String> categoryArray = new ArrayList<String>();
        ArrayList<String> languagesArray = new ArrayList<String>();
		if	(patronym != null && patronym.isChecked()){
            patronymChecked = true;
			typesArray.add("patronym");
		}else patronymChecked = false;

		if (person != null && person.isChecked()){
            personChecked = true;
			typesArray.add("person");
		}else personChecked = false;
		
		if (historians != null && historians.isChecked()){
            historyChecked = true;
			categoryArray.add("history");
		}else historyChecked = false;


        if	(greekl != null && greekl.isChecked()){
            greekChecked = true;
            languagesArray.add("Greek");
        }else greekChecked = false;

        if (latinl != null && latinl.isChecked()){
            latinChecked = true;
            languagesArray.add("Latin");
        }else latinChecked = false;



		if (myth != null && myth.isChecked()){
            mythChecked = true;
			categoryArray.add("myth-literature");
		}else mythChecked = false;

		if	(geographers != null && geographers.isChecked()){
            geographersChecked = true;
			categoryArray.add("geography");
		}else geographersChecked = false;

        if	(inscriptions != null && inscriptions.isChecked()){
            inscriptionsChecked = true;
            categoryArray.add("inscription");
        }else inscriptionsChecked = false;

        if	(nature != null && nature.isChecked()){
            natureChecked = true;
            categoryArray.add("nature");
        }else natureChecked = false;

        if	(philosophy != null && philosophy.isChecked()){
            philosophyChecked = true;
            categoryArray.add("philosophy");
        }else philosophyChecked = false;

        if	(reference != null && reference.isChecked()){
            referenceChecked = true;
            categoryArray.add("reference");
        }else referenceChecked = false;

		listAdapter.filterData(typesArray,categoryArray,languagesArray,selectedAuthorNameTitle);
		hitsNum.setText(listAdapter.totalHits+"\nHits");
	}
	
	private Person readPersonWithName(String personName) {
		// TODO Auto-generated method stub
		
		//routes = new ArrayList<String>();
	    Cursor routesCursor = database.query(TABLE_NAME, new String[] {PERSON_ID,
	            CENTURY,HITS,PERSON_NAME,PERIOD,PERSONID_CUSTOM,WIKILINK}, PERSON_NAME + " = '"+personName+"'", null, null, null, PERSON_NAME);
	        routesCursor.moveToFirst();

            int id = routesCursor.getInt(0);
            String century = routesCursor.getString(1);
            int hits = routesCursor.getInt(2);
            String pName = routesCursor.getString(3);
            String period = routesCursor.getString(4);
            String pID = routesCursor.getString(5);
            String wikiID = routesCursor.getString(6);
            //String image_name = routesCursor.getString(5);
            routesCursor.close();
            return new Person(id,century,hits,pName,period,pID,wikiID);
	        
	}
	
	//Convert pixel to dip 
	public int GetDipsFromPixel(float pixels)
	{
	        // Get the screen's density scale
	        final float scale = getResources().getDisplayMetrics().density;
	        // Convert the dps to pixels, based on density scale
	        return (int) (pixels * scale + 0.5f);
	} 

	@Override
	public void indexReady(ArrayList<IndexTable> theList) {
		progressbar.setVisibility(View.GONE);
		if	 (theList.size() > 0){
			listAdapter = new IndexListAdapter(this, R.layout.index_row,theList);
			indexList.setAdapter(listAdapter);
			listAdapter.notifyDataSetChanged();
			//checkFilters(true);
			indexList.setOnItemClickListener(this);
			myFirstIndexes = theList;

            myAuthorsList.add("ALL");
            for (IndexTable obj : theList) {
                if (!myAuthorsList.contains(obj.getWorkTitle()))
                    myAuthorsList.add(obj.getWorkTitle());
            }
		}
		
	}

    @Override
    public void expandableIndexReady(ArrayList<IndexTable> theList, HashMap<String, ArrayList<IndexTable>> theHashmap) {

    }


    @Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
        if (indexList.getAdapter() != parent.getAdapter()){
            selectedAuthorNameTitle = myAuthorsList.get(position);
            prepareToFilterData();
            popupWindow.dismiss();
        }else{
            //IndexTable ind = myFirstIndexes.get(position);
            IndexTable ind = (IndexTable) indexList.getAdapter().getItem(position);
            IndexListAdapter indexAdapter = (IndexListAdapter)indexList.getAdapter();

            //TextView paragraphNumber = (TextView) view.findViewById(R.id.paragraphnumber);
            Intent intent = new Intent(PeopleIndexTableActivity.this,FullWorkActivity.class);
            intent.putExtra("myWorkName",ind.getWorkTitle());
            intent.putExtra("paragraphID", Integer.toString(ind.getParagraph()));
            //intent.putExtra("personID", currentPerson.getPersonID());
            intent.putExtra("placeName", ind.getName());
            intent.putExtra("hitsList", indexAdapter.getListItems());
            //intent.putExtra("hashmapList", myHashmap);
            intent.putExtra("currentPosition", position);
            //intent.putExtra("childPosition",1);
            intent.putExtra("totalHits", indexList.getAdapter().getCount());
            intent.putExtra("previousActivity","FullWork");
            startActivity(intent);

        }

	}


	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "index clicked group position" +groupPosition +" and child position: "+childPosition, Toast.LENGTH_LONG).show();
		return false;
	}

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
