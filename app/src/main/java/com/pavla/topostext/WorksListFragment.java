package com.pavla.topostext;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.pavla.topostextHelpers.WorksListAdapter;

public class WorksListFragment extends Fragment implements OnClickListener,OnQueryTextListener {
	
	Map<String, Integer> mapIndex;
	ListView worksListView;
	View view;
	SearchView mSearchView;
	ArrayList<String> worksList;
	WorksListAdapter listAdapter;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	        // Inflate the layout for this fragment
			view = inflater.inflate(R.layout.works_fragment, container, false);


		worksList = (ArrayList<String>)getArguments().getSerializable("texts");
			
			
			worksListView = (ListView) view.findViewById(R.id.worksListview);
			MapActivity myActivity = (MapActivity) getActivity();
			mSearchView = myActivity.getSearchView();
			mSearchView.setQuery("",false);//(SearchView) view.findViewById(R.id.searchWorks);
			worksListView.setTextFilterEnabled(true);
			setupSearchView();
			
			if	 (worksList.size() > 0){
				listAdapter = new WorksListAdapter(getActivity(), R.layout.work_name_row,worksList);
				worksListView.setAdapter(listAdapter);
				listAdapter.notifyDataSetChanged(); 
			}
			
			
			worksListView.setOnItemClickListener(new OnItemClickListener() {
		          public void onItemClick(AdapterView<?> parent, View view,
		                  int position, long id) {
		              Intent intent = new Intent(getActivity(),FullWorkActivity.class);
					  String cleanTitle = listAdapter.works_filtered_array.get(position).replace("'","''");
						intent.putExtra("myWorkName",cleanTitle);
						startActivity(intent);
		              }
		            });
			
			getIndexList(worksList);
			displayIndex();
			
			
	        return view;
	    }
	
	private void setupSearchView() {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.clearFocus();
        mSearchView.setQueryHint(getActivity().getResources().getString(R.string.search_works_hint));
    }
	
	private void getIndexList(ArrayList<String> places) {
        mapIndex = new LinkedHashMap<String, Integer>();
        for (int i = 0; i < places.size(); i++) {
			Log.i("LOG","THE PLACE : "+places.get(i));
            String fruit = places.get(i);
            String index = fruit.substring(0, 1);
 
            if (mapIndex.get(index) == null)
                mapIndex.put(index, i);
        }
    }
	
	private void displayIndex() {
        LinearLayout indexLayout = (LinearLayout) view.findViewById(R.id.worksIndexView);
        TextView textView;
        List<String> indexList = new ArrayList<String>(mapIndex.keySet());
        for (String index : indexList) {
            textView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.side_index_item, null);
            //Typeface mycustomFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/NotoSerif-Regular.ttf");
    		//textView.setTypeface(mycustomFont);
            textView.setText(index);
            textView.setOnClickListener(this);
            indexLayout.addView(textView);
        }
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		TextView selectedIndex = (TextView) v;
		worksListView.setSelection(mapIndex.get(selectedIndex.getText()));
	}
	
	@Override
	public boolean onQueryTextChange(String newText) {
        WorksListAdapter myAdapter = (WorksListAdapter) worksListView.getAdapter();
        myAdapter.getFilter().filter(newText);
        return true;
	}

	@Override
	public boolean onQueryTextSubmit(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}
}
