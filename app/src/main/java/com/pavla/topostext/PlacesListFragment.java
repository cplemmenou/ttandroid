package com.pavla.topostext;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.pavla.topostextHelpers.MyListener;
import com.pavla.topostextHelpers.PlacesListAdapter;

import static android.os.Build.VERSION_CODES.M;

public class PlacesListFragment extends Fragment implements OnClickListener,OnQueryTextListener {
	
	Map<String, Integer> mapIndex;
	ListView placesListView;
	View view;
	SearchView mSearchView;
	ArrayList<String> placesList;
	MyListener ml;
	PlacesListAdapter listAdapter;


	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {

		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
	        // Inflate the layout for this fragment
			view = inflater.inflate(R.layout.places_fragment, container, false);

		this.ml = (MyListener) getActivity();
		Log.i("LOG","my listener"+this.ml);

			//placesList = (ArrayList<String>)getArguments().getSerializable("places");
			
			
			 placesListView = (ListView) view.findViewById(R.id.placesListview);
			MapActivity myActivity = (MapActivity) getActivity();
			mSearchView = myActivity.getSearchView();
			mSearchView.setQuery("",false);
			 //mSearchView = (SearchView) view.findViewById(R.id.searchPlaces);
			 placesListView.setTextFilterEnabled(true);
			 setupSearchView();
			
			//if	 (placesList.size() > 0){
				listAdapter = new PlacesListAdapter(getActivity(), R.layout.place_name_row,placesList,getActivity(),ml,placesListView);
				placesListView.setAdapter(listAdapter);
				listAdapter.notifyDataSetChanged(); 
			//}

		if (tabletSize){
			placesListView.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view,
										int position, long id) {
					ml.clickedPlace(listAdapter.places_filtered.get(position));
				}
			});
		}

			
			getIndexList(placesList);
			displayIndex();
			mSearchView.clearFocus();

	        return view;
	    }

	public void updateListAdapter(ArrayList<String> updatedList){
		//Log.i("LOG","list updated with values : "+updatedList);
		//placesList.clear();
		//placesList = (ArrayList<String>) updatedList.clone();
	//	Log.i("LOG","list updated with values : "+placesList);
		listAdapter.notifyDataSetChanged();
	}
	public PlacesListFragment(){


	}
	
	/*public PlacesListFragment(MyListener listxener){
		this.ml = listener;
	}*/
	
	private void getIndexList(ArrayList<String> places) {
        mapIndex = new LinkedHashMap<String, Integer>();
        for (int i = 0; i < places.size(); i++) {
            String fruit = places.get(i);
            String index = fruit.substring(0, 1);
 
            if (mapIndex.get(index) == null)
                mapIndex.put(index, i);
        }
    }
	private void setupSearchView() {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.clearFocus();
        //mSearchView.setFocusableInTouchMode(true);
        mSearchView.setQueryHint(getActivity().getResources().getString(R.string.search_places_hint));
    }
	
	private void displayIndex() {
        LinearLayout indexLayout = (LinearLayout) view.findViewById(R.id.indexView);
        TextView textView;
        List<String> indexList = new ArrayList<String>(mapIndex.keySet());
        for (String index : indexList) {
            textView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.side_index_item, null);
            //Typeface mycustomFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/NotoSerif-Regular.ttf");
    		//textView.setTypeface(mycustomFont);
            textView.setText(index);
            textView.setOnClickListener(this);
            indexLayout.addView(textView);
        }
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		TextView selectedIndex = (TextView) v;
        placesListView.setSelection(mapIndex.get(selectedIndex.getText()));
	}
	
	@Override
	public boolean onQueryTextChange(String newText) {
        PlacesListAdapter myAdapter = (PlacesListAdapter) placesListView.getAdapter();
        myAdapter.getFilter().filter(newText);
        return true;
	}

	@Override
	public boolean onQueryTextSubmit(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}
}
